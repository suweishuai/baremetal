### Baremetal Code with with different architectures

##### Project introduction

Supported architecture types:
- [x] ARMv7
- [x] ARMv8
- [x] rv32
- [x] rv64

##### How to set development environment

- arm32/64
```bash
gcc  : apt-get install gcc-arm-linux-gnueabihf binutils-aarch64-linux-gnu
gdb  : apt-get install gdb-multiarch
qemu : apt-get install qemu-system-arm
```

- riscv32/64
```bash
gcc & gdb  : riscv64-unknown-elf-gcc build from Crosstool-NG : https://blog.csdn.net/u011011827/article/details/120086961 (must be tag crosstool-ng-1.24.0)
qemu       : apt-get install qemu-system-misc
```

##### How to Run

- help
```bash
./do.sh
```

- run

```bash
step1 : build
./do.sh arm64_build
step2 : open qemu
./do.sh run
```

- run with debug

```bash
step1 : build
./do.sh arm64_build
step2 : open qemu
./do.sh debug_run
step3 : gdb
./do.sh debug_gdb
```

##### How to stop

```bash
step1 : send signal to qemu
./do.sh kill
```

##### function list

| item                                      | Architecture correlation | ARM32 | ARM64 | RV32 | RV64 |
| ----------------------------------------- | ------------------------ | ----- | ----- | ---- | ---- |
| uart                                      | Y                        | 1     | 1     | 1    | 1    |
| stdio                                     | N                        | 1     | 1     | 1    | 1    |
| shell                                     | N                        | 1     | 1     | 1    | 1    |
| builtin command                           | N                        | 1     | 1     | 1    | 1    |
| external debug                            | Y                        | 1     | 1     | 1    | 1    |
| Privilege level switching <br />when boot | Y                        | 1     | 1     | 1    | 1    |
| vector table                              | Y                        | 1     | 1     | 1    | 1    |
| calling convention demo                   | Y                        | 1     | 1     | 1    | 1    |
| debug monitor                             | Y                        | 1     | 1     | 1    | 1    |
| svc demo                                  | Y                        | 1     | 1     | 1    | 1    |
| debug monitor : mem_ops                   | Y                        | 1     | 1     | 1    | 1    |
| debug monitor : sysreg_ops                | Y                        | 1     | 1     | 1    | 1    |
| timer interrupt                           | Y                        | 1     | 1     | 1    | 1    |
| atomic instructions test                  | Y                        | 1     | 1     | 1    | 1    |
| spinlock                                  | Y                        | 1     | 1     | 1    | 1    |
| compile optimizer & barrier               | N                        | 1     | 1     | 1    | 1    |
| cpu optimize & memory barrier             | Y                        | 1     | 1     | 1    | 1    |
| cache & mem platform  info                | Y                        | 1     | 1     | 1    | 1    |
| cache ops                                 | Y                        | 1     | 1     | 0    | 0    |
| mmu & page                                | Y                        | 1     | 1     | 1    | 1    |
| stacktrace                                | Y                        | 1     | 1     | 1    | 1    |
