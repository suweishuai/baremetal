#ifndef SHELL_H
#define SHELL_H
#include <stdint.h>

#define ARGC_MAX 4
#define ARGV_LEN_MAX 32

void shell_user(void);
void shell_debug(void);

#endif
