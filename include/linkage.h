/*************************************************************************
  > File Name: linkage.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Mon 13 Jun 2022 11:38:42 AM CST
 ************************************************************************/

#ifndef LINKAGE_H
#define LINKAGE_H

#define __init
#define __initdata
#define __aligned(x)                    __attribute__((__aligned__(x)))

#define BUG_ON(X) do {}while(0)
#define BUG()     do {}while(1)

#endif

