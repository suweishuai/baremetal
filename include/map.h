/*************************************************************************
  > File Name: map.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 12 Jun 2022 12:43:26 AM CST
 ************************************************************************/

#ifndef MAP_H
#define MAP_H

#include "linkage.h"
#include "page.h"

typedef struct map_section {
    unsigned long phys_start_addr;
    unsigned long phys_end_addr;
    unsigned long virt_start_addr;
    unsigned long virt_end_addr;
    unsigned long size;
    unsigned long va_pa_offset;
    unsigned long interval;
    unsigned long count;
} map_section_t;

void arch_map_section(void *pgd_base);

void map_section(void * pgd_base, map_section_t * idmap);

void __init create_pmd_mapping(pmd_t *pmdp,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot);


void __init create_pgd_mapping(pgd_t *pgdp,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot);

void __init pt_ops_set_early(void);

extern pgd_t idmap_pgd[];
extern pud_t idmap_pud[];
extern pmd_t idmap_pmd[];

#define create_l1_mapping create_pgd_mapping

// alloc
extern struct pt_alloc_ops pt_ops;
#define pgd_next_t      pud_t

#define alloc_pgd_next(__va)	pt_ops.alloc_pud(__va)
#define get_pgd_next_virt(__pa)	pt_ops.get_pud_virt(__pa)
#define create_pgd_next_mapping(__nextp, __va, __pa, __sz, __prot)	\
		create_pud_mapping(__nextp, __va, __pa, __sz, __prot)

struct pt_alloc_ops {
	pte_t *(*get_pte_virt)(phys_addr_t pa);
	phys_addr_t (*alloc_pte)(uintptr_t va);
	pmd_t *(*get_pmd_virt)(phys_addr_t pa);
	phys_addr_t (*alloc_pmd)(uintptr_t va);
	pud_t *(*get_pud_virt)(phys_addr_t pa);
	phys_addr_t (*alloc_pud)(uintptr_t va);
};

#endif
