/*************************************************************************
  > File Name: parse.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 01 Apr 2022 04:32:48 PM CST
 ************************************************************************/
#ifndef PARSE_H
#define PARSE_H

#include "cmd.h"

extern fun_t parse(const char * name);
extern fun_t parse_debug(const char * name);

#endif
