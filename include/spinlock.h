/*************************************************************************
  > File Name: spinlock.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Mon 02 May 2022 06:59:04 PM CST
 ************************************************************************/

#ifndef SPIN_LOCK_H
#define SPIN_LOCK_H

#include "arch_spinlock.h"

typedef struct spinlock {
    arch_spinlock_t lock;
} spinlock_t;

static inline void spin_lock(spinlock_t *lock)
{
	arch_spin_lock(&lock->lock);
}

static inline void spin_unlock(spinlock_t *lock)
{
	arch_spin_unlock(&lock->lock);
}
static int spin_trylock(spinlock_t *lock)
{
	arch_spin_trylock(&lock->lock);
}

#define spin_lock_init(_lock)                           \
do {                                                    \
	arch_spin_lock_init(&(_lock)->lock);                \
}while(0)

#endif
