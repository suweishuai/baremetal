#ifndef STDIO_H
#define STDIO_H

#include <stddef.h>

int printf(const char *fmt, ...);
int snprintf(char *buffer, size_t size, const char *fmt, ...);
int puts(const char *s);

#endif
