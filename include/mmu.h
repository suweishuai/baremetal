/*************************************************************************
  > File Name: mmu.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Mon 13 Jun 2022 11:30:05 AM CST
 ************************************************************************/

#ifndef MMU_H
#define MMU_H

void mmu_on(void *pgd_base);
void mmu_off(void);

#endif
