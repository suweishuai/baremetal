#ifndef BARRIER_H
#define BARRIER_H

#include "arch_barrier.h"

#define barrier()   __asm__ __volatile__ ("": : :"memory")
#define wfi()       __asm__ __volatile__ ("wfi" : : : "memory")
#define nop()       __asm__ __volatile__ ("nop")


#ifdef CONFIG_SMP
#define smp_mb()	do { __smp_mb();  } while (0)
#define smp_rmb()	do { __smp_rmb(); } while (0)
#define smp_wmb()	do { __smp_wmb(); } while (0)
#else
#define smp_mb()	barrier()
#define smp_rmb()	barrier()
#define smp_wmb()	barrier()
#endif

#endif
