/*************************************************************************
  > File Name: cmd.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 01 Apr 2022 04:35:46 PM CST
 ************************************************************************/
#ifndef CMD_H
#define CMD_H

#include "shell.h"

extern struct cmd __ucmd_start, __ucmd_end;
extern struct cmd __dcmd_start, __dcmd_end;


typedef void(*fun_t)(int argc, char * argv[]);

struct cmd{
    char name[16];
    fun_t fun;
};

#define TO_STRING(name) #name
#define FUN_NAME(name) name##_fun
#define DFUN_NAME(name) name##_dfun

#define UCMD(name) \
    static void FUN_NAME(name)(int argc, char * argv[]);\
    static struct cmd name __attribute__((section("ucmd"))) __attribute__((aligned(32))) = {\
        TO_STRING(name),\
        FUN_NAME(name),\
    };\
   static void FUN_NAME(name)(int argc, char * argv[])

#define DCMD(name) \
    static void DFUN_NAME(name)(int argc, char * argv[]);\
    static struct cmd name __attribute__((section("dcmd"))) __attribute__((aligned(32))) = {\
        TO_STRING(name),\
        DFUN_NAME(name),\
    };\
   static void DFUN_NAME(name)(int argc, char * argv[])


#endif
