/*************************************************************************
  > File Name: bit_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Tue 10 May 2022 04:00:29 PM CST
 ************************************************************************/

#ifndef BIT_OPS_H
#define BIT_OPS_H

// unsigned long is the size of Processor operation bits
// UL is the size of Processor operation bits
// arm32 rv32 : 4
// arm64 rv64 : 8
#define BIT_MASK(bit_h, bit_l) (((0x1UL<<(1+bit_h-bit_l)) - 0x1UL) << bit_l)

#define REG(a)        (*(volatile unsigned long *)(a))

#define SET_FIELD_BY_ADDR(addr, bit_h, bit_l, value)                        \
    do {                                                                    \
        REG(addr) = ((REG(addr)) & (~(BIT_MASK(bit_h,bit_l)))) |            \
        (((value) << (bit_l)) & (BIT_MASK(bit_h, bit_l)));                  \
    } while (0)

#define GET_FIELD_BY_ADDR(addr, bit_h, bit_l)		(((REG(addr)) & (BIT_MASK(bit_h,bit_l))) >> (bit_l))

#define SET_FIELD_BY_VALUE(val, bit_h, bit_l, value)                        \
    do {                                                                    \
        val = ((val)) & (~(BIT_MASK(bit_h,bit_l)))) |                       \
        (((value) << (bit_l)) & (BIT_MASK(bit_h, bit_l)));                  \
    } while (0)

#define GET_FIELD_BY_VALUE(val, bit_h, bit_l)		(((val) & (BIT_MASK(bit_h,bit_l))) >> (bit_l))

#endif
