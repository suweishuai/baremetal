/*************************************************************************
  > File Name: page.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 12 Jun 2022 07:20:42 PM CST
 ************************************************************************/

#ifndef PAGE_H
#define PAGE_H

#include "arch_page.h"
// "pxx"&pfn
// pxx          : package    of "Value of page table entry"
// pfn          : part(pfn)  of "Value of page table entry"
// pfn          : page frame number of "phy addr"

// 1. check pxx
// pxx_none     : check if pxx is 0

// 2. exchange between "pfn&prot" & "pxx"
// _pxx_pfn     : "pxx"      -> "pfn"
// pfn_pxx      : "pfn&prot" -> "pxx"
// _PAGE_PFN_SHIFT  : shift of pfn in pxx

// 3. exchange between "phy addr" & pxx
// PFN_DOWN     : paddr -> pfn
// PFN_PHYS     " pfn   -> paddr

// 4. exchange between "Value of page table entry" & "pxx"
// pxx_val      : pxx -> "Value of page table entry"
// __pxx        : "Value of page table entry" -> pxx

// 5. get addr offset (from pxx_base) from vaddr
// pxx_index    : part(L0/L1/L2) of vaddr
// PXX_SHIFT    : shift          of L0/L1/L2 (in vaddr)
// PXX_MASK     : mask           of L0/L1/L2


// gen phy from pfn
#define PFN_PHYS(x)	    ((phys_addr_t)(x) << PAGE_SHIFT)
// gen pfn from phy
#define PFN_DOWN(paddr) ((paddr)          >> PAGE_SHIFT)

#define PAGE_SIZE	        (1UL << PAGE_SHIFT)
#define PMD_SIZE            (1UL << PMD_SHIFT )
#define PUD_SIZE            (1UL << PUD_SHIFT )
#define PGD_SIZE            (1UL << PGD_SHIFT )

#define PAGE_MASK	        (~(PAGE_SIZE    - 1))
#define PMD_MASK            (~(PMD_SIZE     - 1))
#define PUD_MASK            (~(PUD_SIZE     - 1))
#define PGD_MASK            (~(PGD_SIZE     - 1))

typedef struct { unsigned long pgprot;  } pgprot_t;
typedef struct { unsigned long pte;     } pte_t;
typedef struct { unsigned long pmd;     } pmd_t;
typedef struct { unsigned long pud;     } pud_t;
typedef struct { unsigned long pgd;     } pgd_t;

// get val
#define pte_val(x)	    ((x).pte)
#define pmd_val(x)      ((x).pmd)
#define pud_val(x)      ((x).pud)
#define pgd_val(x)	    ((x).pgd)
#define pgprot_val(x)	((x).pgprot)

// gen val
#define __pte(x)	((pte_t)    { (x) })
#define __pud(x)    ((pud_t)    { (x) })
#define __pmd(x)    ((pmd_t)    { (x) })
#define __pgd(x)	((pgd_t)    { (x) })
#define __pgprot(x)	((pgprot_t) { (x) })

// index
#define pte_index(vaddr) (((vaddr) >> PAGE_SHIFT)     & (PTRS_PER_PTE - 1))
#define pmd_index(vaddr) (((vaddr) >> PMD_SHIFT)      & (PTRS_PER_PMD - 1))
#define pud_index(vaddr) (((vaddr) >> PUD_SHIFT)      & (PTRS_PER_PUD - 1))
#define pgd_index(vaddr) (((vaddr) >> PGD_SHIFT)      & (PTRS_PER_PGD - 1))

// pxx check
#define pte_none(pte) (pte_val(pte) == 0)
#define pmd_none(pmd) (pmd_val(pmd) == 0)

// gen pxx by pfn & prot
#define pfn_pte(pfn,prot) (__pte((pfn << _PAGE_PFN_SHIFT) | pgprot_val(prot)))
#define pfn_pmd(pfn,prot) (__pmd((pfn << _PAGE_PFN_SHIFT) | pgprot_val(prot)))
#define pfn_pud(pfn,prot) (__pud((pfn << _PAGE_PFN_SHIFT) | pgprot_val(prot)))
#define pfn_pgd(pfn,prot) (__pgd((pfn << _PAGE_PFN_SHIFT) | pgprot_val(prot)))

// get pfn from pxx
#define _pte_pfn(pmd)     (pte_val(pmd) >> _PAGE_PFN_SHIFT)
#define _pmd_pfn(pmd)     (pmd_val(pmd) >> _PAGE_PFN_SHIFT)
#define _pud_pfn(pmd)     (pud_val(pmd) >> _PAGE_PFN_SHIFT)
#define _pgd_pfn(pmd)     (pgd_val(pmd) >> _PAGE_PFN_SHIFT)

#endif
