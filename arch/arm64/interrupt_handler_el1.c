/*************************************************************************
  > File Name: interrupt_handler_el1.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 30 Apr 2022 10:47:20 PM CST
 ************************************************************************/

#include "stdio.h"
#include "pt_regs.h"
#include "gic_v2.h"

typedef void (*interrupt_handler_p)(unsigned long cause,struct pt_regs *pt_regs);

void timer_handler(unsigned long cause,struct pt_regs *pt_regs);

interrupt_handler_p general_int_handler_tab[512]={
    [30] = timer_handler,
};

void el021_irq_handler(struct pt_regs * pt_regs){
    unsigned irq_number = 0;
    irq_number = gic_cpu->C_IAR;
    //printf("irq_number:%d\n",irq_number);
    if (general_int_handler_tab[irq_number] != 0) {
        general_int_handler_tab[irq_number](irq_number,pt_regs);
    }
    gic_cpu->C_EOIR = irq_number;
    return ;
}
