#########################################################################
# File Name: gen_c_code.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Thu 21 Apr 2022 03:33:30 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash

INPUT_FILE=../include/system_register.txt
OUTPUT1_FILE=../include/system_print.h
OUTPUT2_FILE=../include/system_wr_reg.h
OUTPUT3_FILE=../include/system_rd_reg.h

MID1_FILE=reg_ops.tmp

rm ${MID1_FILE}  ${OUTPUT1_FILE} ${OUTPUT2_FILE} ${OUTPUT3_FILE} -f

cat ${INPUT_FILE} | grep "^#" | grep "\.pdf" | while read line
do
    PDF=`echo $line | awk -F " " '{print $2}'`
    PAGE=`echo $line | awk -F " " '{print $3}'`
    echo "printf(STR(${PDF})\"\\t\"STR(${PAGE})\"\\n\");" >> ${OUTPUT1_FILE}
done

cat ${INPUT_FILE} | sed '/^\s*$/d' | sed '/^#/d' | while read line
do
    ATT=`echo $line | awk -F " " '{print $1}'`
    REG=`echo $line | awk -F " " '{print $2}'`

    if [ ${ATT} == RO ];then
        echo "SHOW_SREG(${REG})" >> ${MID1_FILE}
    elif [ ${ATT} == WO ];then
        echo "SET_SREG(${REG})" >> ${MID1_FILE}
    else
        ATT=RW
        echo "SHOW_SREG(${REG})" >> ${MID1_FILE}
        echo "SET_SREG(${REG})" >> ${MID1_FILE}
    fi

    echo "printf(STR(${ATT})\"\\t\"STR(${REG})\"\\n\");" >> ${OUTPUT1_FILE}
done


cat ${MID1_FILE} |grep SET_SREG  | \
    sed '0,/SET_SREG/s/SET_SREG/SET_SREG_BEGIN/' > ${OUTPUT2_FILE}
cat ${MID1_FILE} |grep SHOW_SREG | \
    sed '0,/SHOW_SREG/s/SHOW_SREG/SHOW_SREG_BEGIN/' > ${OUTPUT3_FILE}

rm ${MID1_FILE}
