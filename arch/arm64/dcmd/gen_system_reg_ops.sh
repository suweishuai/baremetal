#########################################################################
# File Name: gen_system_reg_ops.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Thu 21 Apr 2022 03:33:30 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash

INPUT_FILE=../include/system_register.txt
OUTPUT_FILE=../include/system_reg.h

rm ${OUTPUT_FILE} -f

cat << EOF > ${OUTPUT_FILE}
/*automatically generated c include: don't edit*/
#ifndef SYSTEM_REG_H
#define SYSTEM_REG_H

EOF

cat ${INPUT_FILE} | sed '/^\s*$/d' | sed '/^#/d' | while read line
do
    ATT=`echo $line | awk -F " " '{print $1}'`
    REG=`echo $line | awk -F " " '{print $2}'`

    if [ ${ATT} == RO ];then
        echo "#define MRS_${REG}  \"mrs  %0,${REG}\"" >> ${OUTPUT_FILE}
    elif [ ${ATT} == WO ];then
        echo "#define MSR_${REG}  \"msr  ${REG},%0\"" >> ${OUTPUT_FILE}
    else
        ATT=RW
        echo "#define MRS_${REG}  \"mrs  %0,${REG}\"" >> ${OUTPUT_FILE}
        echo "#define MSR_${REG}  \"msr  ${REG},%0\"" >> ${OUTPUT_FILE}
    fi
done

cat << EOF >> ${OUTPUT_FILE}

#endif
EOF
