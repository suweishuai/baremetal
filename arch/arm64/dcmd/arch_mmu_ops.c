/*************************************************************************
  > File Name: arch_mmu_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 11 Jun 2022 12:05:47 PM CST
 ************************************************************************/

#include "barrier.h"
#include "reg_ops.h"

void mmu_on(void *pgd_base){
    unsigned long tmp = 0;
    isb();
    sreg_wr(TTBR0_EL1,pgd_base);
    //sreg_wr(TTBR1_EL1,pgd_base);
    sreg_wr(TCR_EL1,0x3500UL);
    sreg_wr(MAIR_EL1,0xffUL);
    isb();
    tmp = sreg_rd(SCTLR_EL1);
    tmp |= 0x1;
    sreg_wr(SCTLR_EL1,tmp);
    isb();
    nop();
    nop();
    return ;
}

void mmu_off(void){
    isb();
    sreg_wr(SCTLR_EL1,0x0);
    isb();
    nop();
    nop();
    return ;
}
