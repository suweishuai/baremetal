/*************************************************************************
  > File Name: arch_map_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 15 Jun 2022 01:18:59 PM CST
 ************************************************************************/

#include "platform.h"
#include "map.h"
#include "page.h"

void arch_map_section(void *pgd_base){
    unsigned long cbar = 0x08000000;
    map_section_t idmap_gic;

    idmap_gic.size              = SECTION_SIZE;
    idmap_gic.phys_start_addr   = cbar;

    idmap_gic.phys_end_addr     = idmap_gic.phys_start_addr + idmap_gic.size;
    idmap_gic.virt_start_addr   = idmap_gic.phys_start_addr;
    idmap_gic.virt_end_addr     = idmap_gic.phys_end_addr;
    idmap_gic.va_pa_offset      = idmap_gic.virt_start_addr - idmap_gic.phys_start_addr;

    idmap_gic.interval          = SECTION_SIZE;
    idmap_gic.count             = idmap_gic.size/idmap_gic.interval;

    map_section(pgd_base,&idmap_gic);

    return ;
}

