/*************************************************************************
  > File Name: arch_info.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Tue 10 May 2022 04:29:12 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "reg_ops.h"
#include "bit_ops.h"
#include "memory.h"


typedef struct {
    char level_sel;
    char type_sel;
    char name[32];
}cache_t;


void info_cache(void){
    unsigned long CLIDR  = sreg_rd(CLIDR_EL1);
    unsigned long CTR    = sreg_rd(CTR_EL0);
    unsigned long DCZID  = sreg_rd(DCZID_EL0);
    unsigned long CCSIDR = 0;

    printf("CLIDR\t:0X%016lx\n",CLIDR);
    printf("Ctype1\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,2,0));
    printf("Ctype2\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,5,3));
    printf("Ctype3\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,8,6));
    printf("Ctype4\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,11,9));
    printf("Ctype5\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,14,12));
    printf("Ctype6\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,17,15));
    printf("Ctype7\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,20,18));
    printf("LoUIS\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,23,21));
    printf("LoC\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,26,24));
    printf("LoUU\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,29,27));
    printf("ICB\t:0X%016lx\n",GET_FIELD_BY_VALUE(CLIDR,32,30));

    printf("\nCTR\t:0X%016lx\n",CTR);
    printf("IminLine\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,3,0));
    printf("L1Ip\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,15,14));
    printf("DminLine\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,19,16));
    printf("ERG\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,23,20));
    printf("CWG\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,27,24));
    printf("IDC\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,28,28));
    printf("DIC\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,29,29));
    printf("TminLine\t:0X%016lx\n",GET_FIELD_BY_VALUE(CTR,37,32));

    printf("\nDCZID\t:0X%016lx\n",DCZID);
    printf("BS\t:0X%016lx\n",GET_FIELD_BY_VALUE(DCZID,3,0));
    printf("DZP\t:0X%016lx\n",GET_FIELD_BY_VALUE(DCZID,4,4));

    cache_t cache[3]={
        [0]={
            .level_sel = 0,
            .type_sel  = 0,
            .name      = "L1 data cache",
        },
        [1]={
            .level_sel = 0,
            .type_sel  = 1,
            .name      = "L1 instr cache",
        },
        [2]={
            .level_sel = 1,
            .type_sel  = 0,
            .name      = "L2 unified cache",
        },
    };

    int count = sizeof(cache)/sizeof(cache_t);
    int i = 0;

    for(i = 0 ; i < count; i ++){
        printf("\n%s\n",cache[i].name);
        sreg_wr(CSSELR_EL1,(cache[i].level_sel << 1) |(cache[i].type_sel << 0));
        CCSIDR = sreg_rd(CCSIDR_EL1);
        printf("CCSIDR\t:0X%016lx\n",CCSIDR);
        printf("LineSize\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,2,0));
        printf("Associativity\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,12,3));
        printf("NumSets\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,27,13));
        printf("WA\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,28,28));
        printf("RA\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,29,29));
        printf("WB\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,30,30));
        printf("WT\t:0X%016lx\n",GET_FIELD_BY_VALUE(CCSIDR,31,31));
        printf("%ld-way-%ld-set,cache line : 2^%ld Bytes\n",\
                GET_FIELD_BY_VALUE(CCSIDR,12,3)+1,\
                GET_FIELD_BY_VALUE(CCSIDR,27,13)+1,\
                GET_FIELD_BY_VALUE(CCSIDR,2,0)+4);
    }
}

void info_mem(void){
    unsigned int start = PHYS_OFFSET;
    unsigned int size  = PHYS_SIZE;
    printf("range:\t0X%08x-0X%08x\n",start,start+size-1);
    printf("size:\t0X%08XB,\t%dM\n",size,size/1024/1024);
}
