/*************************************************************************
  > File Name: arch_reg_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 21 Apr 2022 12:56:41 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "pt_regs.h"
#include "reg_ops.h"

struct pt_regs * g_pt_regs = NULL;


void show_regs(struct pt_regs * pt_regs)
{
    printf("  x0-x3: %016lx %016lx %016lx %016lx\n", pt_regs->regs[0], pt_regs->regs[1], pt_regs->regs[2], pt_regs->regs[3]);
    printf("  x4-x7: %016lx %016lx %016lx %016lx\n", pt_regs->regs[4], pt_regs->regs[5], pt_regs->regs[6], pt_regs->regs[7]);
    printf(" x8-x11: %016lx %016lx %016lx %016lx\n", pt_regs->regs[8], pt_regs->regs[9], pt_regs->regs[10], pt_regs->regs[11]);
    printf("x12-x15: %016lx %016lx %016lx %016lx\n", pt_regs->regs[12], pt_regs->regs[13], pt_regs->regs[14], pt_regs->regs[15]);
    printf("x16-x19: %016lx %016lx %016lx %016lx\n", pt_regs->regs[16], pt_regs->regs[17], pt_regs->regs[18], pt_regs->regs[19]);
    printf("x20-x23: %016lx %016lx %016lx %016lx\n", pt_regs->regs[20], pt_regs->regs[21], pt_regs->regs[22], pt_regs->regs[23]);
    printf("x24-x27: %016lx %016lx %016lx %016lx\n", pt_regs->regs[24], pt_regs->regs[25], pt_regs->regs[26], pt_regs->regs[27]);
    printf("x28-x30: %016lx %016lx %016lx\n", pt_regs->regs[28], pt_regs->regs[29], pt_regs->regs[30]);
    printf("     sp: %016lx\n", pt_regs->sp);
    printf("     pc: %016lx\n", pt_regs->pc);
    printf(" pstate: %016lx\n", pt_regs->pstate);
    printf("    esr: %016lx\n", pt_regs->esr);
}

DCMD(regs){
    printf("EL0 regs before \"BRK\" instruction :\n");
    show_regs(g_pt_regs);
}


void reg_usage(void){
    printf("Usage : reg wr CurrentEL 0x02\n");
    printf("Usage : reg rd CurrentEL\n");
    printf("available reg:\n");
    #include "system_print.h"
    return ;
}


DCMD(reg){
#define SHOW_SREG_BEGIN(SREG)                                                                           \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%016lx\n", sreg_rd(SREG));return;}
#define SHOW_SREG(SREG)                                                                                 \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%016lx\n", sreg_rd(SREG));return;}
#define SHOW_SREG_END                                                                                   \
        printf("%s : register not supported yet\n",argv[2]);return ;

#define SET_SREG_BEGIN(SREG)                                                                            \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,strtoul(argv[3]));return;}
#define SET_SREG(SREG)                                                                                  \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,strtoul(argv[3]));return;}
#define SET_SREG_END                                                                                    \
    printf("%s : register not supported yet\n",argv[2]);return ;


    // 0   1  2        3
    // reg wr register data // 4
    // reg rd register      // 3
    if (argc < 3){
        reg_usage();
        return ;
    }

    if ( strcmp(argv[1],STR(wr)) == 0 ) {
        if (argc != 4){ reg_usage(); return ; }
        #include "system_wr_reg.h"
        SET_SREG_END
    }else if ( strcmp(argv[1],STR(rd)) == 0 ) {
        if (argc != 3){ reg_usage(); return ; }
        #include "system_rd_reg.h"
        SHOW_SREG_END
    }
    reg_usage();
}
