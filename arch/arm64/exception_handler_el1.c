/*************************************************************************
  > File Name: exception_handler_el1.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 31 Mar 2022 10:36:11 PM CST
 ************************************************************************/
#include "stdio.h"
#include "pt_regs.h"
#include "shell.h"
#include "barrier.h"

void show_regs(struct pt_regs * pt_regs);

static char *m_table[0x10] = {
    [0x00] = "EL0t", //
    [0x04] = "EL1t", // with SP_EL0
    [0x05] = "EL1h", // with SP_EL1
    [0x08] = "EL2t", // with SP_EL0
    [0x09] = "EL2h", // with SP_EL2
};

static char *ec_table[0x40] = {
    [0x00] = "unknown",
    [0x01] = "wf*",
    [0x03] = "c15 mcr/mrc",
    [0x04] = "c15 mcrr/mrrc",
    [0x05] = "c14 mcr/mrc",
    [0x06] = "ldc/stc",
    [0x07] = "FP off",
    [0x08] = "VMRS access",
    [0x09] = "PAC off",
    [0x0a] = "ld/st64b",
    [0x0c] = "c14 mrrc",
    [0x0d] = "branch target",
    [0x0e] = "illegal state",
    [0x11] = "svc in a32",
    [0x12] = "hvc in a32",
    [0x13] = "smc in a32",
    [0x15] = "svc in a64",
    [0x16] = "hvc in a64",
    [0x17] = "smc in a64",
    [0x18] = "other mcr/mrc/sys",
    [0x19] = "SVE off",
    [0x1a] = "eret",
    [0x1c] = "PAC failure",
    [0x20] = "instruction abort (lower)",
    [0x21] = "instruction abort (current)",
    [0x22] = "pc misaligned",
    [0x24] = "data abort (lower)",
    [0x25] = "data abort (current)",
    [0x26] = "sp misaligned",
    [0x28] = "FP exception (a32)",
    [0x2c] = "FP exception (a64)",
    [0x2f] = "SError",
    [0x30] = "BP (lower)",
    [0x31] = "BP (current)",
    [0x32] = "step (lower)",
    [0x33] = "step (current)",
    [0x34] = "watchpoint (lower)",
    [0x35] = "watchpoint (current)",
    [0x38] = "bkpt (a32)",
    [0x3a] = "vector catch (a32)",
    [0x3c] = "brk (a64)",
};


void svc_handler(struct pt_regs * pt_regs){
    printf("%s\n",__func__);
    printf("syscall number is %ld\n",pt_regs->regs[8]);
    return ;
}

void debug_handler(struct pt_regs * pt_regs){
    printf("%s\n",__func__);
    g_pt_regs = pt_regs;
    shell_debug();
    pt_regs->pc += 4;
    return ;
}

void el021_sync_handler(struct pt_regs * pt_regs){
    printf("%s\n",__func__);
    int EC = (pt_regs->esr&0xfc000000)>>26;
    int IL = (pt_regs->esr&0x02000000)>>25;
    int ISS= (pt_regs->esr&0x01ffffff);

    printf("EC :%08X,IL :%08X,ISS :%08X\n",EC,IL,ISS);

    switch(EC){
        case 0X15:{
                      svc_handler(pt_regs);
                      break;
                  }
        case 0X3c:{
                      debug_handler(pt_regs);
                      break;
                  }
        default : {
                      show_regs(pt_regs);
                      wfi();
                  }
    }

    return ;
}
