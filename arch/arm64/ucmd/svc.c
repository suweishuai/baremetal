/*************************************************************************
  > File Name: svc.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 12:27:56 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

UCMD(svc){
    printf("%s\n",__func__);
    asm("LDR X8,=0x1");
    asm("SVC #0x0");
}
