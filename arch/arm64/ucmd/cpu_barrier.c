/*************************************************************************
  > File Name: cpu_barrier.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 06 May 2022 12:20:48 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

#define BINARY_SVC   0xd4000001
#define BINARY_NOP   0xd503201f

UCMD(cpu_optimiz){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_SVC;

    __asm__ __volatile__(
"   adr     %0, 0\n"
"   add     %0, %0, #0xc\n"
"   str     %w1, [%0]\n"
"   nop\n"                    // 0xd503201f TOBE MODIFY to 0xd4000001
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}

UCMD(cpu_barrier){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_SVC;

    __asm__ __volatile__(
"   adr     %0, 0\n"
"   add     %0, %0, #0x10\n"
"   str     %w1, [%0]\n"
"   isb\n"
"   nop\n"                    // 0xd503201f TOBE MODIFY to 0xd4000001
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}
