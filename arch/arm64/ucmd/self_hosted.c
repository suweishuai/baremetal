/*************************************************************************
  > File Name: self_hosted.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Mon 18 Apr 2022 02:54:14 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

UCMD(debug){
    printf("%s\n",__func__);
    asm("BRK #0X0");
    printf("%s exit\n",__func__);
}
