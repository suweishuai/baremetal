/*************************************************************************
  > File Name: test.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 01 May 2022 08:50:39 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"
#include "spinlock.h"
#include "string.h"

/*************************atomic*************************************/

void test_atomic(void){

    unsigned res1 = 10,val1 = 10;
    unsigned res2 = 10,val2 = 10;
    unsigned mem;
    unsigned fail_flag = 0;
    // scene 1 AB12
    __asm__ __volatile__(
"   ldxr   %w0, [%4]\n"
"   stxr   %w0, %w5, [%4]\n"
"   ldr     %2, [%4]\n"
"   ldxr   %w1, [%4]\n"
"   stxr   %w1, %w6, [%4]\n"
"   ldr     %3, [%4]\n"
    : "=&r" (res1),"=&r" (res2),"=&r" (val1),"=&r" (val2)
    : "r" (&mem), "r" (1) , "r" (2)
    : "cc");

    //printf("res1:%d,val1:%d\n",res1,val1); // 0 1
    //printf("res2:%d,val2:%d\n",res2,val2); // 0 2

    if ( ( res1 == 0 ) && ( res2 == 0 ) && ( val1 == 1 ) && ( val2 == 2 ) )
        printf("atomic sence1 pass\n");
    else
        {printf("atomic sence1 fail\n"); fail_flag ++;}

    // scene 2 A1B2
    res1 = 10;val1 = 10;res2 = 10;val2 = 10;
    __asm__ __volatile__(
"   ldxr   %w0, [%4]\n"
"   ldxr   %w1, [%4]\n"
"   stxr   %w0, %w5, [%4]\n"
"   ldr     %2, [%4]\n"
"   stxr   %w1, %w6, [%4]\n"
"   ldr     %3, [%4]\n"
    : "=&r" (res1),"=&r" (res2),"=&r" (val1),"=&r" (val2)
    : "r" (&mem), "r" (1) , "r" (2)
    : "cc");

    //printf("res1:%d,val1:%d\n",res1,val1); // 0 1
    //printf("res2:%d,val2:%d\n",res2,val2); // 1 1

    if ( ( res1 == 0 ) && ( res2 == 1 ) && ( val1 == 1 ) && ( val2 == 1 ) )
        printf("atomic sence2 pass\n");
    else
        {printf("atomic sence2 fail\n"); fail_flag ++;}

    // scene 3 A12B
    res1 = 10;val1 = 10;res2 = 10;val2 = 10;
    __asm__ __volatile__(
"   ldxr   %w0, [%4]\n"
"   ldxr   %w1, [%4]\n"
"   stxr   %w1, %w6, [%4]\n"
"   ldr     %3, [%4]\n"
"   stxr   %w0, %w5, [%4]\n"
"   ldr     %2, [%4]\n"
    : "=&r" (res1),"=&r" (res2),"=&r" (val1),"=&r" (val2)
    : "r" (&mem), "r" (1) , "r" (2)
    : "cc");

    //printf("res1:%d,val1:%d\n",res1,val1); // 1 2
    //printf("res2:%d,val2:%d\n",res2,val2); // 0 2

    if ( ( res1 == 1 ) && ( res2 == 0 ) && ( val1 == 2 ) && ( val2 == 2 ) )
        printf("atomic sence3 pass\n");
    else
        {printf("atomic sence3 fail\n"); fail_flag ++;}

    // scene 4 B&2 happen the same time
    // TODO


    if ( fail_flag == 0 )
        printf("atomic pass\n");
    else
        printf("atomic fail\n");

    return ;
}


/*************************spinlock***********************************/

spinlock_t spinlock;
volatile unsigned test_spinlock_test = 0;
volatile unsigned test_spinlock_get_pass_count = 0;
volatile unsigned test_spinlock_get_fail_count = 0;

void test_spinlock(void){

    int i = 0;

    spin_lock_init(&spinlock);
    spin_lock(&spinlock);
    test_spinlock_test = 1;


    while(1){
        for(i = 0;i < 100;i++);
        if (test_spinlock_get_pass_count != 0){
            printf("spinlock test fail\n");
            break;
        }

        if ( test_spinlock_get_fail_count == 100 ){
            printf("spinlock test pass\n");
            break;
        }
    }


    test_spinlock_test = 0;
    test_spinlock_get_pass_count = 0;
    test_spinlock_get_fail_count = 0;
    spin_unlock(&spinlock);
}

/*************************UCMD test***********************************/

UCMD(test){
    if (argc != 2){
        printf("Usage for function test :\n");
        printf("\ttest atomic\n");
        printf("\ttest spinlock\n");
        return ;
    }
    if (strcmp(argv[1],"atomic") == 0){
        test_atomic();
    }else if (strcmp(argv[1],"spinlock") == 0){
        test_spinlock();
    }
}
