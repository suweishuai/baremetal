/*************************************************************************
  > File Name: cache_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 15 May 2022 03:32:26 PM CST
 ************************************************************************/

#ifndef CACHE_OPS_H
#define CACHE_OPS_H

#include "reg_ops.h"

#define icache_on()  sreg_wr( SCTLR_EL1 , sreg_rd(SCTLR_EL1) |  (1UL<<12) )
#define icache_off() sreg_wr( SCTLR_EL1 , sreg_rd(SCTLR_EL1) & ~(1UL<<12) )

#define dcache_on()  sreg_wr( SCTLR_EL1 , sreg_rd(SCTLR_EL1) |  (1UL<<2) )
#define dcache_off() sreg_wr( SCTLR_EL1 , sreg_rd(SCTLR_EL1) & ~(1UL<<2) )

#define ucache_on()  dcache_on()
#define ucache_off() dcache_off()

#endif

