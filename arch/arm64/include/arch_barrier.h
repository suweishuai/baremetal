#ifndef ARCH_BARRIER_H
#define ARCH_BARRIER_H

#define dmb(opt)	__asm__ __volatile__("dmb " #opt : : : "memory")
#define dsb(opt)	__asm__ __volatile__("dsb " #opt : : : "memory")
#define isb()		__asm__ __volatile__("isb" : : : "memory")

#define mb()		dsb(sy)
#define rmb()		dsb(ld)
#define wmb()		dsb(st)

#define __smp_mb()	dmb(ish)
#define __smp_rmb()	dmb(ishld)
#define __smp_wmb()	dmb(ishst)

#endif
