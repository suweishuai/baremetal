#ifndef ARCH_STACKSTRACE_H
#define ARCH_STACKSTRACE_H

inline __attribute__((always_inline)) void * get_current_stack_frame(void){
    void* fp;
    asm volatile ("mov %0, x29" : "=r" (fp));
    return fp;
}

inline __attribute__((always_inline)) void * get_caller_stack_frame(void * current_fp)
{
    void* caller_fp;
    caller_fp =  (void *)(*(unsigned long int *)(current_fp));
    return caller_fp;
}

inline __attribute__((always_inline)) void * get_callee_return_address(void * current_fp)
{
    void* callee_ret;
    callee_ret =  (void *)(*(unsigned long int *)((char *)(current_fp + sizeof(void *))));
    return callee_ret;
}

#endif
