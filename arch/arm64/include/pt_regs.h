/*************************************************************************
  > File Name: pt_regs.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 04:20:00 PM CST
 ************************************************************************/

#ifndef PT_REGS_H
#define PT_REGS_H

extern struct pt_regs * g_pt_regs;

struct pt_regs {
    unsigned long int regs[31];
    unsigned long int sp;
    unsigned long int pc;
    unsigned long int pstate;
    unsigned long int esr;
    unsigned long int reserved;
};

#endif
