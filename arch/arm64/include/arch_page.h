/*************************************************************************
  > File Name: arch_page.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 03:53:10 PM CST
 ************************************************************************/

#ifndef ARCH_PAGE_H
#define ARCH_PAGE_H

#include "types.h"

#define SECTION_SIZE (1024*1024*2)

// SHIFT SIZE & MASK
#define PAGE_SHIFT	        (12)
#define PMD_SHIFT           (21)
#define PUD_SHIFT           (30)
#define PGD_SHIFT           (39)

// PER
#define PTRS_PER_PTE    (PAGE_SIZE / sizeof(pte_t))
#define PTRS_PER_PMD    (PAGE_SIZE / sizeof(pmd_t))
#define PTRS_PER_PUD    (PAGE_SIZE / sizeof(pud_t))
#define PTRS_PER_PGD    (PAGE_SIZE / sizeof(pgd_t))


// prot level1
#define _PAGE_PRESENT   (1 << 0)
#define _PAGE_TABLE     (3 << 0)
#define _PAGE_ATTR      (0 << 2) // index MAIR
#define _PAGE_AP        (0 << 6) // Access Permissions
#define _PAGE_SHARED    (3 << 8)
#define _PAGE_AF        (1 << 10)

// prot level2
#define _PAGE_KERNEL  (_PAGE_PRESENT      \
				      | _PAGE_AF          \
				      | _PAGE_AP          \
                      | _PAGE_ATTR)

#define PAGE_KERNEL_EXEC	__pgprot(_PAGE_KERNEL)
#define PAGE_TABLE          __pgprot(_PAGE_TABLE)

// PFN OPS
#define _PAGE_PFN_SHIFT 12

#endif
