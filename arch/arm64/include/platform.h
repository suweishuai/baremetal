/*************************************************************************
  > File Name: platform.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Tue 12 Apr 2022 04:35:45 PM CST
 ************************************************************************/

#ifndef PLATFORM_H
#define PLATFORM_H

#include "uart.h"
#define UART_BASE 0x09000000
#define UART    ((UART_RefDef *)(UART_BASE))

#include "timer.h"
#define TIMER_BASE SYS_REG
#define TIMECMP    CNTP_CVAL_EL0
#define TIME       CNTPCT_EL0
#define TIMCTL     CNTP_CTL_EL0
#define TIMESUB    CNTP_TVAL_EL0

#include "gic_v2.h"

#endif /* PLATFORM_H */
