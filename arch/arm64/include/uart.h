#ifndef UART_H
#define UART_H

#define __IO   volatile
typedef struct
{
    __IO unsigned int UARTDR; // 0x00
    __IO unsigned int REG2;   // 0x04
    __IO unsigned int REG3;   // 0x08
    __IO unsigned int REG4;   // 0x0C
    __IO unsigned int REG5;   // 0x10
    __IO unsigned int REG6;   // 0x14
    __IO unsigned int UARTFR; // 0x18
    __IO unsigned int REG8;   // 0x1C

} UART_RefDef;


unsigned short int getchar(void);
int putchar(int ch);
int uart_init(void);

#endif /* UART_H */
