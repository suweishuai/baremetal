.section ".text.vector", "ax"

.balign 0x800 // because the low [10:0] of the VBAR_EL1 is Reserved
              // The .balign directive provides the same alignment
              // functionality as .align with a consistent behavior
              // across all architectures
// vector table for EL1
.global vec_tbl_el1
vec_tbl_el1:

.balign 0x80
curr_el_sp0_sync:
    WFI

.balign 0x80
curr_el_sp0_irq:
    WFI

.balign 0x80
curr_el_sp0_fiq:
    WFI

.balign 0x80
curr_el_sp0_serr:
    WFI

.balign 0x80
curr_el_spx_sync:
    MRS X1, ESR_EL1
    WFI

.balign 0x80
curr_el_spx_irq:
    WFI

.balign 0x80
curr_el_spx_fiq:
    WFI

.balign 0x80
curr_el_spx_serr:
    WFI


.balign 0x80
lower_el_64b_sync: // svc from el0 & el1 is 64bit
    sub sp,sp,8
    str x30, [sp]

    bl lower_el_64b_sync2

    ldr	x30, [sp]

    add sp,sp,8
    ERET

.balign 0x80
lower_el_64b_irq:
    sub sp,sp,8
    str x30, [sp]

    bl lower_el_64b_irq2

    ldr	x30, [sp]

    add sp,sp,8
    ERET

.balign 0x80
lower_el_64b_fiq:
    WFI

.balign 0x80
lower_el_64b_serr:
    WFI

.balign 0x80
lower_el_32b_sync:
    WFI

.balign 0x80
lower_el_32b_irq:
    WFI

.balign 0x80
lower_el_32b_fiq:
    WFI

.balign 0x80
lower_el_32b_serr:
    WFI
