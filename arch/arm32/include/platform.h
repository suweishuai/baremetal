/*************************************************************************
  > File Name: platform.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Tue 12 Apr 2022 04:35:45 PM CST
 ************************************************************************/

#ifndef PLATFORM_H
#define PLATFORM_H

#include "uart.h"
#define UART_BASE 0x09000000
#define UART    ((UART_RefDef *)(UART_BASE))

#include "timer.h"
#define TIMER_BASE CP15_REG
#define TIMECMP    CNTP_CVAL
#define TIME       CNTPCT
#define TIMCTL     CNTP_CTL
#define TIMESUB    CNTP_TVAL

#include "gic_v2.h"

#endif /* PLATFORM_H */
