#ifndef TIMER_H
#define TIMER_H

#define MTIMECMP_OFFSET NULL
#define MTIME_OFFSET    NULL

// timer frequency : 62.5MHZ
#define SYSTEM_FREQ     0x03b9aca0 // 62500000 // CNTFRQ
// interrupt frequency : 100
#define TIMER_PERIOD    SYSTEM_FREQ/100

int timer_init(void);
unsigned long get_time(void);

#endif /* TIMER_H */
