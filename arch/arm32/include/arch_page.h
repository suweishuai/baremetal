/*************************************************************************
  > File Name: arch_page.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 03:53:10 PM CST
 ************************************************************************/

#ifndef ARCH_PAGE_H
#define ARCH_PAGE_H

#include "types.h"

#define SECTION_SIZE (1024*1024*1)

// SHIFT SIZE & MASK
#define PAGE_SHIFT	        (12)
#define PMD_SHIFT           (20)
#define PUD_SHIFT           (20)
#define PGD_SHIFT           (20)

// PER
#define PTRS_PER_PTE    (PAGE_SIZE / sizeof(pte_t))
#define PTRS_PER_PMD    (PAGE_SIZE / sizeof(pmd_t))
#define PTRS_PER_PUD    (PAGE_SIZE / sizeof(pud_t))
#define PTRS_PER_PGD    (4*PAGE_SIZE / sizeof(pgd_t))


// prot level1
#define _PAGE_SECTION   (2 << 0)
#define _PAGE_DOMAIN    (0 << 5)  // index DACR
#define _PAGE_AP        (3 << 10) // Access Permissions

// prot level2
#define _PAGE_KERNEL  (_PAGE_SECTION      \
                      | _PAGE_DOMAIN      \
                      | _PAGE_AP)


#define PAGE_KERNEL_EXEC	__pgprot(_PAGE_KERNEL)
#define PAGE_TABLE          __pgprot(0x1)

// PFN OPS
#define _PAGE_PFN_SHIFT 12

#endif
