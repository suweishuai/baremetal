/*************************************************************************
  > File Name: mem_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 02 Apr 2022 06:48:00 PM CST
 ************************************************************************/

#ifndef MEM_OPS_H
#define MEM_OPS_H

#include <stdint.h>

#define BIT(x)                 (1UL << (x))
#define MASK(x)                (BIT(x) - 1)
#define GENMASK(msb, lsb)      ((BIT((msb + 1) - (lsb)) - 1) << (lsb))
#define _FIELD_LSB(field)      ((field) & ~(field - 1))
#define FIELD_PREP(field, val) ((val) * (_FIELD_LSB(field)))
#define FIELD_GET(field, val)  (((val) & (field)) / _FIELD_LSB(field))

#define ALIGN_UP(x, a)   (((x) + ((a)-1)) & ~((a)-1))
#define ALIGN_DOWN(x, a) ((x) & ~((a)-1))

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

static inline uint32_t read32(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile("ldr\t%r0, [%1]" : "=r"(data) : "r"(addr) : "memory");
    return data;
}

static inline void write32(uint32_t addr, uint32_t data)
{
    __asm__ volatile("str\t%r0, [%1]" : : "r"(data), "r"(addr) : "memory");
}

static inline uint32_t writeread32(uint32_t addr, uint32_t data)
{
    write32(addr, data);
    return read32(addr);
}

static inline uint32_t set32(uint32_t addr, uint32_t set)
{
    uint32_t data;
    __asm__ volatile("ldr\t%r0, [%1]\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstr\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set)
                     : "memory");
    return data;
}

static inline uint32_t clear32(uint32_t addr, uint32_t clear)
{
    uint32_t data;
    __asm__ volatile("ldr\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r2\n"
                     "\tstr\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(clear)
                     : "memory");
    return data;
}

static inline uint32_t mask32(uint32_t addr, uint32_t clear, uint32_t set)
{
    uint32_t data;
    __asm__ volatile("ldr\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r3\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstr\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set), "r"(clear)
                     : "memory");
    return data;
}

static inline uint16_t read16(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile("ldrh\t%r0, [%1]" : "=r"(data) : "r"(addr) : "memory");
    return data;
}

static inline void write16(uint32_t addr, uint16_t data)
{
    __asm__ volatile("strh\t%r0, [%1]" : : "r"(data), "r"(addr) : "memory");
}

static inline uint16_t set16(uint32_t addr, uint16_t set)
{
    uint16_t data;
    __asm__ volatile("ldrh\t%r0, [%1]\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstrh\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set)
                     : "memory"

    );
    return data;
}

static inline uint16_t clear16(uint32_t addr, uint16_t clear)
{
    uint16_t data;
    __asm__ volatile("ldrh\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r2\n"
                     "\tstrh\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(clear)
                     : "memory");
    return data;
}

static inline uint16_t mask16(uint32_t addr, uint16_t clear, uint16_t set)
{
    uint16_t data;
    __asm__ volatile("ldrh\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r3\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstrh\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set), "r"(clear)
                     : "memory");
    return data;
}

static inline uint16_t writeread16(uint32_t addr, uint16_t data)
{
    write16(addr, data);
    return read16(addr);
}

static inline uint8_t read8(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile("ldrb\t%r0, [%1]" : "=r"(data) : "r"(addr) : "memory");
    return data;
}

static inline void write8(uint32_t addr, uint8_t data)
{
    __asm__ volatile("strb\t%r0, [%1]" : : "r"(data), "r"(addr) : "memory");
}

static inline uint8_t set8(uint32_t addr, uint8_t set)
{
    uint8_t data;
    __asm__ volatile("ldrb\t%r0, [%1]\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstrb\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set)
                     : "memory");
    return data;
}

static inline uint8_t clear8(uint32_t addr, uint8_t clear)
{
    uint8_t data;
    __asm__ volatile("ldrb\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r2\n"
                     "\tstrb\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(clear)
                     : "memory");
    return data;
}

static inline uint8_t mask8(uint32_t addr, uint8_t clear, uint8_t set)
{
    uint8_t data;
    __asm__ volatile("ldrb\t%r0, [%1]\n"
                     "\tbic\t%r0, %r0, %r3\n"
                     "\torr\t%r0, %r0, %r2\n"
                     "\tstrb\t%r0, [%1]"
                     : "=&r"(data)
                     : "r"(addr), "r"(set), "r"(clear)
                     : "memory");
    return data;
}

static inline uint8_t writeread8(uint32_t addr, uint8_t data)
{
    write8(addr, data);
    return read8(addr);
}
#endif
