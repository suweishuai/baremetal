/*************************************************************************
  > File Name: arch_spinlock.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 01 May 2022 08:35:26 PM CST
 ************************************************************************/

#ifndef ARCH_SPIN_LOCK_H
#define ARCH_SPIN_LOCK_H

typedef unsigned int arch_spinlock_t;


# define arch_spin_lock_init(lock)				        \
	do { *(lock) = 0; } while (0)

#define arch_spin_is_locked(x) (*x)

static inline int arch_spin_trylock(arch_spinlock_t *lock)
{
    int busy = 0;

    __asm__ __volatile__(
    "ldrex   %0, [%1]\n"
    "teq %0, #0\n"
    "strexeq %0, %2, [%1]"
    : "=&r" (busy)
    : "r" (lock), "r" (1)
    : "cc");

    return !busy;

}

static inline void arch_spin_lock(arch_spinlock_t *lock)
{
    while (1) {
        if (arch_spin_is_locked(lock))
            continue;

        if (arch_spin_trylock(lock))
            break;
    }
}

static inline void arch_spin_unlock(arch_spinlock_t *lock)
{

    __asm__ __volatile__(
    "str %1, [%0]\n"
    :
    : "r" (lock), "r" (0)
    : "cc");
}

#endif
