/*************************************************************************
  > File Name: reg_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 02 Apr 2022 04:37:29 PM CST
 ************************************************************************/

#ifndef REG_OPS_H
#define REG_OPS_H

#include <stdint.h>


// barrier
#define sysop(op) __asm__ volatile(op ::: "memory")

// string
#define STR(X) #X
#define _concat(a, _1, b, ...) a##b
#define _sr_tkn_S(_0, _1, op0, op1, CRn, CRm, op2) s##op0##_##op1##_c##CRn##_c##CRm##_##op2
#define _sr_tkn(a) a
#define sr_tkn(...) _concat(_sr_tkn, __VA_ARGS__, )(__VA_ARGS__)


// system mrs/msr reg rw ops
#define __mrs(reg)                                                                                 \
    ({                                                                                             \
        uint32_t val;                                                                              \
        __asm__ volatile("mrs\t%0, " #reg : "=r"(val));                                            \
        val;                                                                                       \
    })
#define _mrs(reg) __mrs(reg)

#define __msr(reg, val)                                                                            \
    ({                                                                                             \
        uint32_t __val = (uint32_t)val;                                                            \
        __asm__ volatile("msr\t" #reg ", %0" : : "r"(__val));                                      \
    })
#define _msr(reg, val) __msr(reg, val)

#define mrs(reg)      _mrs(sr_tkn(reg))
#define msr(reg, val) _msr(sr_tkn(reg), val)

#define psr_rd(reg) mrs(reg)
#define psr_wr(reg, val) msr(reg, val)


#define msr_sync(reg, val)                                                                         \
    ({                                                                                             \
        _msr(sr_tkn(reg), val);                                                                    \
        sysop("isb");                                                                              \
    })

// system mrc/mcr reg rw ops


#include "copr_reg.h"

#define __mrc(reg)                                                                                 \
    ({                                                                                             \
        uint32_t val;                                                                              \
        __asm__ volatile(                                                                          \
                MRC_##reg                                                                          \
                : "=r"(val));                                                                      \
        val;                                                                                       \
    })
#define _mrc(reg) __mrc(reg)

#define __mcr(reg, val)                                                                            \
    ({                                                                                             \
        uint32_t __val = (uint32_t)val;                                                            \
        __asm__ volatile(                                                                          \
                MCR_##reg                                                                          \
                :                                                                                  \
                : "r"(__val));                                                                     \
    })
#define _mcr(reg, val) __mcr(reg, val)

#define mrc(reg)      _mrc(sr_tkn(reg))
#define mcr(reg, val) _mcr(sr_tkn(reg), val)

#define sreg_rd(reg) mrc(reg)
#define sreg_wr(reg, val) mcr(reg, val)

#define mcr_sync(reg, val)                                                                         \
    ({                                                                                             \
        _mcr(sr_tkn(reg), val);                                                                    \
        sysop("isb");                                                                              \
    })


// system mrrc/mcrr reg64 rw ops

#define __mrrc(reg)                                                                                \
    ({                                                                                             \
        uint32_t Rt;                                                                               \
        uint32_t Rt2;                                                                              \
        __asm__ volatile(                                                                          \
                MRRC_##reg                                                                         \
                : "=r"(Rt), "=r"(Rt2));                                                            \
        ((uint64_t)Rt2 << 32 | Rt) ;                                                               \
    })
#define _mrrc(reg) __mrrc(reg)

#define __mcrr(reg, val)                                                                           \
    ({                                                                                             \
        uint32_t Rt  = (uint32_t)val;                                                              \
        uint32_t Rt2 = (uint32_t)(val >> 32);                                                      \
        __asm__ volatile(                                                                          \
                MCRR_##reg                                                                         \
                :                                                                                  \
                : "r"(Rt), "r"(Rt2));                                                              \
    })
#define _mcrr(reg, val) __mcrr(reg, val)

#define mrrc(reg)      _mrrc(sr_tkn(reg))
#define mcrr(reg, val) _mcrr(sr_tkn(reg), val)

#define sreg64_rd(reg) mrrc(reg)
#define sreg64_wr(reg, val) mcrr(reg, val)


// system reg bit rw ops
#define reg_clr(reg, bits)      _msr(sr_tkn(reg), _mrs(sr_tkn(reg)) & ~(bits))
#define reg_set(reg, bits)      _msr(sr_tkn(reg), _mrs(sr_tkn(reg)) | bits)
#define reg_mask(reg, clr, set) _msr(sr_tkn(reg), (_mrs(sr_tkn(reg)) & ~(clr)) | set)

#define reg_clr_sync(reg, bits)                                                                    \
    ({                                                                                             \
        reg_clr(sr_tkn(reg), bits);                                                                \
        sysop("isb");                                                                              \
    })
#define reg_set_sync(reg, bits)                                                                    \
    ({                                                                                             \
        reg_set(sr_tkn(reg), bits);                                                                \
        sysop("isb");                                                                              \
    })
#define reg_mask_sync(reg, clr, set)                                                               \
    ({                                                                                             \
        reg_mask(sr_tkn(reg), clr, set);                                                           \
        sysop("isb");                                                                              \
    })

#endif
