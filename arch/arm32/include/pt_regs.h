/*************************************************************************
  > File Name: pt_regs.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 01:27:42 PM CST
 ************************************************************************/

#ifndef PT_REGS_H
#define PT_REGS_H

extern struct pt_regs * g_pt_regs;

struct pt_regs {
    unsigned int regs[13];
    unsigned int usr_sp;
    unsigned int usr_lr;
    unsigned int usr_ret_pc;
    unsigned int usr_psr;
    unsigned int svc_psr;
    unsigned int svc_sp;
};

#endif
