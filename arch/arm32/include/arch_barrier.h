#ifndef ARCH_BARRIER_H
#define ARCH_BARRIER_H

#define dmb(opt) __asm__ __volatile__ ("dmb " #opt : : : "memory")
#define dsb(opt) __asm__ __volatile__ ("dsb " #opt : : : "memory")
#define isb(opt) __asm__ __volatile__ ("isb " #opt : : : "memory")

#define mb()		dsb()
#define rmb()		dsb()
#define wmb()		dsb(st)

#define __smp_mb()	dmb(ish)
#define __smp_rmb()	dmb(ish)
#define __smp_wmb()	dmb(ishst)

#endif
