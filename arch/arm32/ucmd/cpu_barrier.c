/*************************************************************************
  > File Name: cpu_barrier.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 06 May 2022 12:20:48 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

#define BINARY_SVC   0xdf00
#define BINARY_NOP   0xbf00

UCMD(cpu_optimiz){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_SVC;

    __asm__ __volatile__(
"   mov     %0, PC\n"
"   strh    %1, [%0]\n"
"   nop\n"                    // 0xbf00 TOBE MODIFY to 0xdf00
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}

UCMD(cpu_barrier){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_SVC;

    __asm__ __volatile__(
"   mov     %0, PC\n"
"   add     %0, %0, #0x8\n"
"   strh    %1, [%0]\n"
"   isb \n"
"   nop\n"                    // 0xbf00 TOBE MODIFY to 0xdf00
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}
