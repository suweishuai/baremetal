/*************************************************************************
  > File Name: arch_reg_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 21 Apr 2022 12:56:41 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "pt_regs.h"
#include "reg_ops.h"

struct pt_regs * g_pt_regs = NULL;


void show_regs(struct pt_regs * pt_regs)
{
    printf("  x0-x3: %08x %08x %08x %08x\n", pt_regs->regs[0], pt_regs->regs[1], pt_regs->regs[2], pt_regs->regs[3]);
    printf("  x4-x7: %08x %08x %08x %08x\n", pt_regs->regs[4], pt_regs->regs[5], pt_regs->regs[6], pt_regs->regs[7]);
    printf(" x8-x11: %08x %08x %08x %08x\n", pt_regs->regs[8], pt_regs->regs[9], pt_regs->regs[10], pt_regs->regs[11]);
    printf("    x12: %08x\n", pt_regs->regs[12]);
    printf(" usr_sp: %08x\n", pt_regs->usr_sp);
    printf(" usr_lr: %08x\n", pt_regs->usr_lr);
    printf(" pl1_sp: %08x\n", pt_regs->svc_sp);
    printf("pl1_psr: %08x\n", mrs(cpsr));
    printf("usr_psr: %08x\n", pt_regs->usr_psr);
    printf("usr_ret_pc: %08x  , should sub 2 to return\n", pt_regs->usr_ret_pc);
}

DCMD(regs){
    printf("PL0 regs before \"BKPT\" instruction :\n");
    show_regs(g_pt_regs);
}


void reg_usage(void){
    printf("Usage : reg wr cpsr 0x02\n");
    printf("Usage : reg rd cpsr\n");
    printf("available reg:\n");
    printf(STR(RW)"\t"STR(cpsr)"\n");
    printf(STR(RW)"\t"STR(spsr)"\n");
    #include "copr_print.h"
    return ;
}


DCMD(reg){
#define SHOW_SREG_BEGIN(SREG)                                                                           \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%08x\n", sreg_rd(SREG));return;}
#define SHOW_SREG(SREG)                                                                                 \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%08x\n", sreg_rd(SREG));return;}
#define SHOW_SREG_END                                                                                   \
        printf("%s : register not supported yet\n",argv[2]);return ;

#define SET_SREG_BEGIN(SREG)                                                                            \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,(uint32_t)strtoul(argv[3]));return;}
#define SET_SREG(SREG)                                                                                  \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,(uint32_t)strtoul(argv[3]));return;}
#define SET_SREG_END                                                                                    \
    printf("%s : register not supported yet\n",argv[2]);return ;


    // 0   1  2        3
    // reg wr register data // 4
    // reg rd register      // 3
    if (argc < 3){
        reg_usage();
        return ;
    }

    if (strcmp(argv[2],STR(cpsr)) == 0 || strcmp(argv[2],STR(spsr)) == 0){ // psr_op
        if ( strcmp(argv[1],STR(wr)) == 0 ) {
            if (argc != 4){ reg_usage(); return ; }
            if ( strcmp(argv[2],STR(cpsr)) == 0 ) { psr_wr(cpsr,(uint32_t)strtoul(argv[3])); return;}
            else if ( strcmp(argv[2],STR(spsr)) == 0 ) {psr_wr(spsr,(uint32_t)strtoul(argv[3]));return;}
            printf("%s : register not supported yet\n",argv[2]);return ;
        }else if ( strcmp(argv[1],STR(rd)) == 0  ) {
            if (argc != 3){ reg_usage(); return ; }
            if ( strcmp(argv[2],STR(cpsr)) == 0 ) {printf(STR(cpsr)": 0x%08x\n", psr_rd(cpsr));return;}
            else if ( strcmp(argv[2],STR(spsr)) == 0 ) {printf(STR(spsr)": 0x%08x\n", psr_rd(spsr));return;}
            printf("%s : register not supported yet\n",argv[2]);return ;
        }
    }else{ // sreg_op
        if ( strcmp(argv[1],STR(wr)) == 0 ) {
            if (argc != 4){ reg_usage(); return ; }
            #include "copr_wr_reg.h"
            SET_SREG_END
        }else if ( strcmp(argv[1],STR(rd)) == 0 ) {
            if (argc != 3){ reg_usage(); return ; }
            #include "copr_rd_reg.h"
            SHOW_SREG_END
        }
    }
    reg_usage();
}
