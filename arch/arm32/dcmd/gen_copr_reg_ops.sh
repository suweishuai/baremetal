#########################################################################
# File Name: gen_copr_reg_ops.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Thu 21 Apr 2022 03:33:30 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash

INPUT_FILE=../include/copr_register.txt
OUTPUT_FILE=../include/copr_reg.h

rm ${OUTPUT_FILE} -f

cat << EOF > ${OUTPUT_FILE}
/*automatically generated c include: don't edit*/
#ifndef COPR_REG_H
#define COPR_REG_H

EOF

cat ${INPUT_FILE} | sed '/^\s*$/d' | sed '/^#/d' | while read line
do
    BIT=`echo $line | awk -F " " '{print $1}'`

    if [ $BIT == 32 ];then
        Coproc=`echo $line | awk -F " " '{print $2}'`
        CRn=`echo $line | awk -F " " '{print $3}'`
        Opc1=`echo $line | awk -F " " '{print $4}'`
        CRm=`echo $line | awk -F " " '{print $5}'`
        Opc2=`echo $line | awk -F " " '{print $6}'`
        ATT=`echo $line | awk -F " " '{print $7}'`
        REG=`echo $line | awk -F " " '{print $8}'`

        if [ ${ATT} == RO ];then
            echo "#define MRC_${REG}  \"mrc ${Coproc},${Opc1},%0,${CRn},${CRm},${Opc2}\"" >> ${OUTPUT_FILE}
        elif [ ${ATT} == WO ];then
            echo "#define MCR_${REG}  \"mcr ${Coproc},${Opc1},%0,${CRn},${CRm},${Opc2}\"" >> ${OUTPUT_FILE}
        else
            ATT=RW
            echo "#define MRC_${REG}  \"mrc ${Coproc},${Opc1},%0,${CRn},${CRm},${Opc2}\"" >> ${OUTPUT_FILE}
            echo "#define MCR_${REG}  \"mcr ${Coproc},${Opc1},%0,${CRn},${CRm},${Opc2}\"" >> ${OUTPUT_FILE}
        fi
    elif [ $BIT == 64 ];then
        Coproc=`echo $line | awk -F " " '{print $2}'`
        CRn=`echo $line | awk -F " " '{print $3}'`
        Opc1=`echo $line | awk -F " " '{print $4}'`
        CRm=`echo $line | awk -F " " '{print $5}'`
        Opc2=`echo $line | awk -F " " '{print $6}'`
        ATT=`echo $line | awk -F " " '{print $7}'`
        REG=`echo $line | awk -F " " '{print $8}'`

        if [ ${ATT} == RO ];then
            echo "#define MRRC_${REG}  \"mrrc ${Coproc},${CRm},%0,%1,${Opc1}\"" >> ${OUTPUT_FILE}
        elif [ ${ATT} == WO ];then
            echo "#define MCRR_${REG}  \"mcrr ${Coproc},${CRm},%0,%1,${Opc1}\"" >> ${OUTPUT_FILE}
        else
            ATT=RW
            echo "#define MRRC_${REG}  \"mrrc ${Coproc},${CRm},%0,%1,${Opc1}\"" >> ${OUTPUT_FILE}
            echo "#define MCRR_${REG}  \"mcrr ${Coproc},${CRm},%0,%1,${Opc1}\"" >> ${OUTPUT_FILE}
        fi

    fi
done

cat << EOF >> ${OUTPUT_FILE}

#endif
EOF
