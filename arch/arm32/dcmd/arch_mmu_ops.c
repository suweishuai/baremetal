/*************************************************************************
  > File Name: arch_mmu_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 11 Jun 2022 12:05:47 PM CST
 ************************************************************************/

#include "barrier.h"
#include "reg_ops.h"

void mmu_on(void *pgd_base){
    isb();
    sreg_wr(TTBR0,pgd_base);
    sreg_wr(DACR,0xFFFFFFFF);
    sreg_wr(SCTLR,0x1);
    isb();
    return ;
}

void mmu_off(void){
    isb();
    sreg_wr(SCTLR,0x0);
    isb();
    return ;
}
