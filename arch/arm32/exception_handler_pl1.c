/*************************************************************************
  > File Name: exception_handler_pl1.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 01:15:56 PM CST
 ************************************************************************/

#include "stdio.h"
#include "pt_regs.h"
#include "shell.h"
#include "barrier.h"

typedef void (*exception_handler_p)(unsigned long addr,struct pt_regs *pt_regs);
void debug_handler(unsigned long addr,struct pt_regs *pt_regs);
void show_regs(struct pt_regs * pt_regs);

exception_handler_p general_exc_handler_tab[32]={
    [2] = debug_handler,
};

int svc_handler(struct pt_regs * pt_regs){

    printf("syscall number is %d\n",pt_regs->regs[8]);

    return 0;
}

void prefetch_abort_handler(unsigned long addr, unsigned int ifsr, struct pt_regs *pt_regs){
    printf("abort handler,cause:%d\n",ifsr & 0x3f);

    if (general_exc_handler_tab[ifsr&0x3f] != 0) {
        general_exc_handler_tab[ifsr&0x3f](addr,pt_regs);
    }else{
        show_regs(pt_regs);
        wfi();
    }
}

void debug_handler(unsigned long addr,struct pt_regs *pt_regs){
    printf("%s\n",__func__);
    g_pt_regs = pt_regs;
    shell_debug();
    return ;
}
