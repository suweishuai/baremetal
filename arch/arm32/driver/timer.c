/*************************************************************************
  > File Name: timer.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 27 Apr 2022 04:10:31 PM CST
 ************************************************************************/

#include "platform.h"
#include "reg_ops.h"
#include "pt_regs.h"
#include "spinlock.h"

#define TIMER_USE_TVAL

int timer_init(void){
#ifdef TIMER_USE_TVAL
    sreg_wr(CNTP_TVAL,TIMER_PERIOD);
#else
    sreg64_wr(CNTP_CVAL,sreg64_rd(CNTPCT)+TIMER_PERIOD);
#endif
    sreg_wr(CNTP_CTL, (1 << 0) | (0 << 1));
    return 0;
}

void timer_handler(unsigned long cause,struct pt_regs *pt_regs){
#ifdef TIMER_USE_TVAL
    sreg_wr(CNTP_TVAL,TIMER_PERIOD);
#else
    sreg64_wr(CNTP_CVAL,sreg64_rd(CNTPCT)+TIMER_PERIOD);
#endif

    extern spinlock_t spinlock;
    extern volatile unsigned test_spinlock_test;
    extern volatile unsigned test_spinlock_get_pass_count;
    extern volatile unsigned test_spinlock_get_fail_count;
    if ( test_spinlock_test ){
        if ( 1 == spin_trylock(&spinlock) ){
            test_spinlock_get_pass_count ++ ;
        }{
            test_spinlock_get_fail_count ++ ;
        }
    }

    return ;
}

unsigned long get_time(void){
    return sreg64_rd(CNTPCT);
}
