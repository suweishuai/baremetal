/*************************************************************************
  > File Name: gic_v2.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 28 Apr 2022 04:18:56 PM CST
 ************************************************************************/

#include "platform.h"
#include "reg_ops.h"
#include "gic_v2.h"

gic_v2_dist_t * gic_dist  = 0;
gic_v2_cpu_t  * gic_cpu   = 0;

int gic_v2_init(void){
    int i = 0;
    unsigned cbar = 0;

    cbar =  sreg_rd(CBAR);

    gic_dist = (gic_v2_dist_t *)cbar;
    gic_cpu  = (gic_v2_cpu_t  *)(cbar+0x10000);

#if 0
    // The GICD_IPRIORITYRs provide an 8-bit priority field for each interrupt supported by the GIC.
    for(i = 0 ; i < 32; i++){
        gic_dist->D_IPRIORITYR[i] = 0x5 << 3;
    }
#endif

    gic_dist->D_IGROUPR[0] = (GRP1 << NON_SECURE_PT);

    // Provides an interrupt priority filter.
    gic_cpu->C_PMR = 0XF8;

#if 0
    // The value of this field controls how the 8-bit interrupt priority field is split into a group priority field
    // 0X2
    // Group priority field : Subpriority field
    // [7:3]                : [2:0]
    // ggggg                : sss
    gic_cpu->C_BPR = 0X2;
#endif


    // The GICD_ISENABLERs provide a Set-enable bit for each interrupt supported by the GIC.
    // Writing 1 to a Set-enable bit enables forwarding of the corresponding interrupt from the
    // Distributor to the CPU interfaces.
    gic_dist->D_ISENABLER[0] =  (1 << NON_SECURE_PT);

    // Enables the forwarding of pending interrupts from the Distributor to the CPU interfaces.
    gic_dist->D_CTLR = (1 << GRP1);

    // Enables the signaling of interrupts by the CPU interface to the connected processor
    gic_cpu->C_CTLR =  (1 << GRP1);

    return 0;
}
