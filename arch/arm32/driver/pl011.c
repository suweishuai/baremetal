#include "platform.h"

unsigned short int getchar(void){
    while ((UART->UARTFR & 0x00000010) != 0);
    return (0xFF & (UART->UARTDR));
}

int putchar(int ch){
    UART->UARTDR = ch;
    while ((UART->UARTFR & 0x00000080) == 0);
    return 0;
}

int uart_init(void){
    return 0;
}
