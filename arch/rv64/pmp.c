/*************************************************************************
  > File Name: pmp.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: 2023年06月10日 星期六 18时09分05秒
 ************************************************************************/

#include "pmp.h"
#include "stdint.h"

void pmp_setup(void)
{
  unsigned long int pmpc = PMP_A_NAPOT | PMP_R | PMP_W | PMP_X;
  asm volatile (
                "csrw pmpaddr0, %1\n\t"
                "csrw pmpcfg0, %0\n\t"
                :
                : "r" (pmpc), "r" (-1UL)
                :
                );
}
