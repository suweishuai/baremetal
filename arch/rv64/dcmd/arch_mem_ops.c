/*************************************************************************
  > File Name: arch_mem_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 20 Apr 2022 11:23:09 AM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"

#include "mem_ops.h"

#define FUNCTION_WR(name) void name (char *argv1,char *argv2)
#define FUNCTION_RD(name) void name (char *argv1)

FUNCTION_WR(wr64){

    uint64_t addr  = (uint64_t)strtoul(argv1);
    uint64_t data  = (uint64_t)strtoul(argv2);
    write64(addr,data);
}

FUNCTION_RD(rd64){

    uint64_t addr  = (uint64_t)strtoul(argv1);
    uint64_t data = 0;
    data = read64(addr);
    printf("0x%016lx\n",data);
}
