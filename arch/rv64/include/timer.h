#ifndef TIMER_H
#define TIMER_H

#define MTIMECMP_OFFSET 0x4000
#define MTIME_OFFSET    0xbff8

// timer frequency : 10MHZ
#define SYSTEM_FREQ     10000000 // CNTFRQ
// interrupt frequency : 100
#define TIMER_PERIOD    10000000/100

int timer_init(void);
unsigned long get_time(void);

#endif /* TIMER_H */
