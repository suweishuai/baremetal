/*************************************************************************
  > File Name: platform.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Tue 12 Apr 2022 04:35:45 PM CST
 ************************************************************************/

#ifndef PLATFORM_H
#define PLATFORM_H

#include "uart.h"
#define UART_BASE 0x10000000
#define UART    ((UART_RefDef *)(UART_BASE))

#define GENERAL_EXCEPTION_NUMBER 16
#define GENERAL_INTERRUPT_NUMBER 16

#include "timer.h"
#define TIMER_BASE 0x02000000
#define MTIMECMP  *(volatile unsigned long *)(TIMER_BASE+MTIMECMP_OFFSET)
#define MTIME     *(volatile unsigned long *)(TIMER_BASE+MTIME_OFFSET)

#endif /* PLATFORM_H */
