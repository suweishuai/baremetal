/*************************************************************************
  > File Name: pt_regs.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 09:18:55 PM CST
 ************************************************************************/

#ifndef PT_REGS_H
#define PT_REGS_H

extern struct pt_regs * g_pt_regs;

struct pt_regs {
	union {
		struct {
			long x1;
			long x4;
			long x5;
			long x6;
			long x7;
			long x10;
			long x11;
			long x12;
			long x13;
			long x14;
			long x15;
			long x16;
			long x17;
			long x28;
			long x29;
			long x30;
			long x31;
		};
		long caller_regs[17];
	};
	long mepc;
	long mstatus;
};

#endif
