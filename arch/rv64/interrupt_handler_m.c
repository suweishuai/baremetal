/*************************************************************************
  > File Name: interrupt_handler_m.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 30 Apr 2022 11:11:15 PM CST
 ************************************************************************/

#include "pt_regs.h"
#include "stdio.h"
#include "rv_xlen.h"
#include "platform.h"

typedef void (*interrupt_handler_p)(struct pt_regs *pt_regs);

void mtimer_handler(struct pt_regs *pt_regs);

interrupt_handler_p general_int_handler_tab[16]={
    [7] = mtimer_handler,
};
