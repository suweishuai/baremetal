/*************************************************************************
  > File Name: timer.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 27 Apr 2022 04:10:31 PM CST
 ************************************************************************/

#include "platform.h"
#include "reg_ops.h"
#include "pt_regs.h"
#include "spinlock.h"

int timer_init(void){
    MTIMECMP = MTIME + TIMER_PERIOD;
    sreg_wr(mie, 1 << 7);
    sreg_wr(mstatus, 0x00000008);
    return 0;
}

void mtimer_handler(struct pt_regs *pt_regs){
    MTIMECMP = MTIME + TIMER_PERIOD;

    extern spinlock_t spinlock;
    extern volatile unsigned test_spinlock_test;
    extern volatile unsigned test_spinlock_get_pass_count;
    extern volatile unsigned test_spinlock_get_fail_count;
    if ( test_spinlock_test ){
        if ( 1 == spin_trylock(&spinlock) ){
            test_spinlock_get_pass_count ++ ;
        }{
            test_spinlock_get_fail_count ++ ;
        }
    }

    return ;
}

unsigned long get_time(void){
    return MTIME;
}
