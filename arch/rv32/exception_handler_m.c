/*************************************************************************
  > File Name: exception_handler_m.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 09:17:04 PM CST
 ************************************************************************/

#include "pt_regs.h"
#include "stdio.h"
#include "rv_xlen.h"
#include "platform.h"
#include "shell.h"
#include "barrier.h"


typedef void (*exception_handler_p)(struct pt_regs *pt_regs);
void ecall_handler(struct pt_regs *pt_regs);
void debug_handler(struct pt_regs *pt_regs);
void show_regs(struct pt_regs * pt_regs);


typedef void (*interrupt_handler_p)(struct pt_regs *pt_regs);
extern interrupt_handler_p general_int_handler_tab[];

exception_handler_p general_exc_handler_tab[16]={
    [3] = debug_handler,
    [8] = ecall_handler,
};

void trap_handler(unsigned long cause, struct pt_regs *pt_regs) {
    if (!(cause & MCAUSE_INT) && ((cause & MCAUSE_CAUSE) < GENERAL_EXCEPTION_NUMBER)) {
        if (general_exc_handler_tab[cause] != 0) {
            general_exc_handler_tab[cause](pt_regs);
        }
    }else if((cause & MCAUSE_INT) && ((cause & MCAUSE_CAUSE) < GENERAL_INTERRUPT_NUMBER)) {
        //printf("irq_number:%d\n",cause & MCAUSE_CAUSE);
        if (general_int_handler_tab[cause&MCAUSE_CAUSE] != 0) {
            general_int_handler_tab[cause&MCAUSE_CAUSE](pt_regs);
        }
    }else{
        printf("trap cause : 0x%08x\n",cause);
        show_regs(pt_regs);
        wfi();
    }
}


void debug_handler(struct pt_regs *pt_regs){
    printf("%s\n",__func__);
    g_pt_regs = pt_regs;
    shell_debug();
    pt_regs->mepc += 4;
    return ;
}

void ecall_handler(struct pt_regs *pt_regs){
    pt_regs->mepc += 4;
    printf("syscall number is %d\n",pt_regs->x17);
    return ;
}
