/*************************************************************************
  > File Name: cache_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 15 May 2022 03:32:26 PM CST
 ************************************************************************/

#ifndef CACHE_OPS_H
#define CACHE_OPS_H

#include "reg_ops.h"

#define icache_on()
#define icache_off()

#define dcache_on()
#define dcache_off()

#define ucache_on()
#define ucache_off()

#endif

