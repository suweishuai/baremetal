/*************************************************************************
  > File Name: memory.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 01:16:27 PM CST
 ************************************************************************/

#ifndef MEMORY_H
#define MEMORY_H

#define PHYS_OFFSET 0x80000000
#define PHYS_SIZE   0x08000000

#endif
