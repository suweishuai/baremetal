/*************************************************************************
  > File Name: reg_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 21 Apr 2022 06:41:08 PM CST
 ************************************************************************/

#ifndef REG_OPS_H
#define REG_OPS_H

#include <stdint.h>

// string
#define STR(X) #X
#define _concat(a, _1, b, ...) a##b
#define _sr_tkn(a) a
#define sr_tkn(...) _concat(_sr_tkn, __VA_ARGS__, )(__VA_ARGS__)

// csr
#include "system_reg.h"
#define __read_csr(reg)                                                                            \
    ({                                                                                             \
        uint32_t val;                                                                              \
        __asm__ volatile(                                                                          \
        CSRR_##reg                                                                                 \
        : "=r"(val));                                                                              \
        val;                                                                                       \
    })
#define _read_csr(reg)          __read_csr(reg)

#define __write_csr(reg, val)                                                                      \
    ({                                                                                             \
        uint32_t __val = (uint32_t)val;                                                            \
        __asm__ volatile(                                                                          \
        CSRW_##reg                                                                                 \
        :                                                                                          \
        : "r"(__val));                                                                             \
    })
#define _write_csr(reg, val)    __write_csr(reg, val)

#define read_csr(reg)           _read_csr(sr_tkn(reg))
#define write_csr(reg, val)     _write_csr(sr_tkn(reg), val)

#define sreg_rd(reg) read_csr(reg)
#define sreg_wr(reg, val) write_csr(reg, val)
#endif
