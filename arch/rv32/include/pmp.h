/*************************************************************************
  > File Name: pmp.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: 2023年06月10日 星期六 18时12分12秒
 ************************************************************************/
#ifndef PMP_H
#define PMP_H

#define PMP_R				0x01UL
#define PMP_W				0x02UL
#define PMP_X				0x04UL
#define PMP_A				0x18UL
#define PMP_A_TOR			0x08UL
#define PMP_A_NA4			0x10UL
#define PMP_A_NAPOT			0x18UL
#define PMP_L				0x80UL


#endif

