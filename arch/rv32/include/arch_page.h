/*************************************************************************
  > File Name: arch_page.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 03:53:10 PM CST
 ************************************************************************/

#ifndef ARCH_PAGE_H
#define ARCH_PAGE_H

#include "types.h"

#define SECTION_SIZE (1024*1024*4)

// SHIFT SIZE & MASK
#define PAGE_SHIFT	        (12)
#define PMD_SHIFT           (22)
#define PUD_SHIFT           (22)
#define PGD_SHIFT           (22)

// PER
#define PTRS_PER_PTE    (PAGE_SIZE / sizeof(pte_t))
#define PTRS_PER_PMD    (PAGE_SIZE / sizeof(pmd_t))
#define PTRS_PER_PUD    (PAGE_SIZE / sizeof(pud_t))
#define PTRS_PER_PGD    (PAGE_SIZE / sizeof(pgd_t))


// prot level1
#define _PAGE_PRESENT   (1 << 0)
#define _PAGE_READ      (1 << 1)    /* Readable */
#define _PAGE_WRITE     (1 << 2)    /* Writable */
#define _PAGE_EXEC      (1 << 3)    /* Executable */
#define _PAGE_USER      (1 << 4)    /* User */
#define _PAGE_GLOBAL    (1 << 5)    /* Global */
#define _PAGE_ACCESSED  (1 << 6)    /* Set by hardware on any access */
#define _PAGE_DIRTY     (1 << 7)    /* Set by hardware on any write */
#define _PAGE_SOFT      (1 << 8)    /* Reserved for software */

// prot level2
#define _PAGE_TABLE   _PAGE_PRESENT
#define _PAGE_KERNEL (_PAGE_READ        \
				    | _PAGE_WRITE       \
				    | _PAGE_PRESENT     \
				    | _PAGE_ACCESSED    \
				    | _PAGE_DIRTY       \
				    | _PAGE_GLOBAL)

#define PAGE_KERNEL_EXEC	__pgprot(_PAGE_KERNEL | _PAGE_EXEC | _PAGE_USER)
#define PAGE_TABLE          __pgprot(_PAGE_TABLE)

// PFN OPS
#define _PAGE_PFN_SHIFT 10

#endif
