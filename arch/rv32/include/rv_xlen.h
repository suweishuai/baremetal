/*************************************************************************
  > File Name: rv_xlen.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 09:56:48 PM CST
 ************************************************************************/

#ifndef RV_XLEN_H
#define RV_XLEN_H


// instructions & data type
#if __riscv_xlen == 64
# define SLL32    sllw
# define STORE    sd
# define LOAD     ld
# define LWU      lwu
# define LOG_REGBYTES 3
#else
# define SLL32    sll
# define STORE    sw
# define LOAD     lw
# define LWU      lw
# define LOG_REGBYTES 2
#endif

#define REGBYTES (1 << LOG_REGBYTES)


//exception
#if __riscv_xlen == 64
#define MCAUSE_INT          0x8000000000000000UL
#define MCAUSE_CAUSE        0x7FFFFFFFFFFFFFFFUL
#else
#define MCAUSE_INT          0x80000000UL
#define MCAUSE_CAUSE        0x7FFFFFFFUL
#endif


#endif
