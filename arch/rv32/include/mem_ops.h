/*************************************************************************
  > File Name: mem_ops.h
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 02 Apr 2022 06:48:00 PM CST
 ************************************************************************/

#ifndef MEM_OPS_H
#define MEM_OPS_H

#include <stdint.h>

#define BIT(x)                 (1UL << (x))
#define MASK(x)                (BIT(x) - 1)
#define GENMASK(msb, lsb)      ((BIT((msb + 1) - (lsb)) - 1) << (lsb))
#define _FIELD_LSB(field)      ((field) & ~(field - 1))
#define FIELD_PREP(field, val) ((val) * (_FIELD_LSB(field)))
#define FIELD_GET(field, val)  (((val) & (field)) / _FIELD_LSB(field))

#define ALIGN_UP(x, a)   (((x) + ((a)-1)) & ~((a)-1))
#define ALIGN_DOWN(x, a) ((x) & ~((a)-1))

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

static inline uint32_t read32(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile(
            "lw %0, (%1)"
            : "=r"(data)
            : "r"(addr)
            : "memory");
    return data;
}

static inline void write32(uint32_t addr, uint32_t data)
{
    __asm__ volatile(
            "sw %0, (%1)"
            :
            : "r"(data), "r"(addr)
            : "memory");
}

static inline uint32_t writeread32(uint32_t addr, uint32_t data)
{
    write32(addr, data);
    return read32(addr);
}

static inline uint16_t read16(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile(
            "lhu %0, (%1)"
            : "=r"(data)
            : "r"(addr)
            : "memory");
    return data;
}

static inline void write16(uint32_t addr, uint16_t data)
{
    __asm__ volatile(
            "sh %0, (%1)"
            :
            : "r"(data), "r"(addr)
            : "memory");
}

static inline uint16_t writeread16(uint32_t addr, uint16_t data)
{
    write16(addr, data);
    return read16(addr);
}

static inline uint8_t read8(uint32_t addr)
{
    uint32_t data;
    __asm__ volatile(
            "lbu %0, (%1)"
            : "=r"(data)
            : "r"(addr)
            : "memory");
    return data;
}

static inline void write8(uint32_t addr, uint8_t data)
{
    __asm__ volatile(
            "sb %0, (%1)"
            :
            : "r"(data), "r"(addr)
            : "memory");
}

static inline uint8_t writeread8(uint32_t addr, uint8_t data)
{
    write8(addr, data);
    return read8(addr);
}
#endif
