#ifndef UART_H
#define UART_H

#define __IO   volatile
typedef struct
{
    __IO unsigned char THR_RBR; 	// 0x00
    __IO unsigned char IER;    		// 0x01
    __IO unsigned char IIR_FCR;     // 0x02
    __IO unsigned char LCR;    		// 0x03
    __IO unsigned char MCR;    		// 0x04
    __IO unsigned char LSR;     	// 0x05
    __IO unsigned char MSR; 		// 0x06
    __IO unsigned char SCR;    		// 0x07

} UART_RefDef;




/* Line status */
#define LSR_DR			0x01 /* Receiver Data ready */
#define LSR_OE			0x02 /* Overrun error */
#define LSR_PE			0x04 /* Parity error */
#define LSR_FE			0x08 /* Framing error */
#define LSR_BI			0x10 /* Break interrupt */
#define LSR_THRE		0x20 /* Transmitter holding register empty */
#define LSR_TEMT		0x40 /* Transmitter empty */
#define LSR_EIRF		0x80 /* Error in RCVR FIFO */

unsigned short int getchar(void);
int putchar(int ch);
int uart_init(void);

#endif /* UART_H */
