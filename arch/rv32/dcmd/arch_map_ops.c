/*************************************************************************
  > File Name: arch_map_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 15 Jun 2022 01:21:09 PM CST
 ************************************************************************/

#include "platform.h"
#include "map.h"
#include "page.h"

void arch_map_section(void *pgd_base){
    map_section_t idmap_timer;

    idmap_timer.size              = SECTION_SIZE;
    idmap_timer.phys_start_addr   = TIMER_BASE;

    idmap_timer.phys_end_addr     = idmap_timer.phys_start_addr + idmap_timer.size;
    idmap_timer.virt_start_addr   = idmap_timer.phys_start_addr;
    idmap_timer.virt_end_addr     = idmap_timer.phys_end_addr;
    idmap_timer.va_pa_offset      = idmap_timer.virt_start_addr - idmap_timer.phys_start_addr;

    idmap_timer.interval          = SECTION_SIZE;
    idmap_timer.count             = idmap_timer.size/idmap_timer.interval;

    map_section(pgd_base,&idmap_timer);

    return ;
}

