/*************************************************************************
  > File Name: arch_reg_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Thu 21 Apr 2022 12:56:41 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "pt_regs.h"
#include "reg_ops.h"

struct pt_regs * g_pt_regs = NULL;


void show_regs(struct pt_regs * pt_regs)
{
    printf("     x1: %08x\n", pt_regs->x1);
    printf("  x4-x7: %08x %08x %08x %08x\n", pt_regs->x4, pt_regs->x5, pt_regs->x6, pt_regs->x7);
    printf("x10-x13: %08x %08x %08x %08x\n", pt_regs->x10, pt_regs->x11, pt_regs->x12, pt_regs->x13);
    printf("x14-x17: %08x %08x %08x %08x\n", pt_regs->x14, pt_regs->x15, pt_regs->x16, pt_regs->x17);
    printf("x28-x31: %08x %08x %08x\n", pt_regs->x28, pt_regs->x29, pt_regs->x30,pt_regs->x31);
    printf("   mepc: %08x\n", pt_regs->mepc);
    printf("mstatus: %08x\n", pt_regs->mstatus);
}

DCMD(regs){
    printf("U-mode regs before \"EBREAK\" instruction :\n");
    show_regs(g_pt_regs);
}


void reg_usage(void){
    printf("Usage : reg wr mstatus 0x02\n");
    printf("Usage : reg rd mstatus\n");
    printf("available reg:\n");
    #include "system_print.h"
    return ;
}


DCMD(reg){
#define SHOW_SREG_BEGIN(SREG)                                                                           \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%08x\n", sreg_rd(SREG));return;}
#define SHOW_SREG(SREG)                                                                                 \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {printf(STR(SREG)": 0x%08x\n", sreg_rd(SREG));return;}
#define SHOW_SREG_END                                                                                   \
        printf("%s : register not supported yet\n",argv[2]);return ;

#define SET_SREG_BEGIN(SREG)                                                                            \
    if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,(uint32_t)strtoul(argv[3]));return;}
#define SET_SREG(SREG)                                                                                  \
    else if ( strcmp(argv[2],STR(SREG)) == 0 ) {sreg_wr(SREG,(uint32_t)strtoul(argv[3]));return;}
#define SET_SREG_END                                                                                    \
    printf("%s : register not supported yet\n",argv[2]);return ;


    // 0   1  2        3
    // reg wr register data // 4
    // reg rd register      // 3
    if (argc < 3){
        reg_usage();
        return ;
    }

    if ( strcmp(argv[1],STR(wr)) == 0 ) {
        if (argc != 4){ reg_usage(); return ; }
        #include "system_wr_reg.h"
        SET_SREG_END
    }else if ( strcmp(argv[1],STR(rd)) == 0 ) {
        if (argc != 3){ reg_usage(); return ; }
        #include "system_rd_reg.h"
        SHOW_SREG_END
    }
    reg_usage();
}
