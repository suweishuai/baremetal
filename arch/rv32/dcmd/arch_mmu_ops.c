/*************************************************************************
  > File Name: arch_mmu_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sat 11 Jun 2022 10:25:24 PM CST
 ************************************************************************/

#include "barrier.h"
#include "reg_ops.h"

void mmu_on(void *pgd_base){
    unsigned long satp_value = 0;
    satp_value |= (0x8UL << 28);
    satp_value |= ((unsigned long)pgd_base >> 12);
    asm("sfence.vma");
    sreg_wr(satp,satp_value);
    asm("sfence.vma");
    nop();
    return ;
}

void mmu_off(void){
    asm("sfence.vma");
    sreg_wr(satp,0);
    asm("sfence.vma");
    nop();
    return ;
}
