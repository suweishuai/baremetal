/*************************************************************************
  > File Name: arch_info.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 11 May 2022 12:09:53 PM CST
 ************************************************************************/

#include "stdio.h"
#include "memory.h"

void info_cache(void){
    printf("rv have no cache info\n");
}

void info_mem(void){
    unsigned int start = PHYS_OFFSET;
    unsigned int size  = PHYS_SIZE;
    printf("range:\t0X%08x-0X%08x\n",start,start+size-1);
    printf("size:\t0X%08XB,\t%dM\n",size,size/1024/1024);
}
