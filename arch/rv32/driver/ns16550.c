#include "platform.h"

unsigned short int getchar(void){
    while ( (UART->LSR & LSR_DR) == 0 );
    return (0xFF & (UART->THR_RBR));
}

int putchar(int ch){
    UART->THR_RBR = ch;
    while ( (UART->LSR & LSR_TEMT) == 0 );
    return 0;
}

int uart_init(void){
    return 0;
}
