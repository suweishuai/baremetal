/*************************************************************************
  > File Name: svc.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 17 Apr 2022 12:27:56 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

UCMD(svc){
    printf("%s\n",__func__);
    asm("li a7,1");
    asm("ecall");
}
