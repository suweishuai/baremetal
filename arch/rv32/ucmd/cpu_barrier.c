/*************************************************************************
  > File Name: cpu_barrier.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 06 May 2022 12:20:48 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"

#define BINARY_ECALL 0x00000073
#define BINARY_NOP   0x00000013

UCMD(cpu_optimiz){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_ECALL;

    __asm__ __volatile__(
"   auipc   %0, 0\n"
"   addi    %0, %0, 0xc\n"
"   sw      %1, (%0)\n"
"   nop\n"                    // 0x00000013 TOBE MODIFY to 0x00000073
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}

UCMD(cpu_barrier){
    printf("%s\n",__func__);

    unsigned addr  = 0;
	unsigned value = BINARY_ECALL;

    __asm__ __volatile__(
"   auipc   %0, 0\n"
"   addi    %0, %0, 0x10\n"
"   sw      %1, (%0)\n"
"   fence.i\n"
"   nop\n"                    // 0x00000013 TOBE MODIFY to 0x00000073
"   nop\n"
    :
    : "r" (addr), "r" (value)
    : "cc");

    printf("%s end\n",__func__);
}
