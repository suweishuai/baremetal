#include "platform.h"
#include "stdio.h"
#include "generated/autoconf.h"
#include "shell.h"

void main(void)
{
    uart_init();
    putchar('H');
    putchar('e');
    putchar('l');
    putchar('l');
    putchar('o');
    putchar(' ');
    putchar('W');
    putchar('o');
    putchar('r');
    putchar('l');
    putchar('d');
    putchar('\n');
    printf("HELLO AGAIN,I AM %s\n",ARCH);
    shell_user();
    return;
}
