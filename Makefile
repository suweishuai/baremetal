include arch/Makefile.build
-include include/config/auto.conf

OBJ=baremetal

help:
	@echo make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-

auto.conf:
	@echo ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE)
	@echo -------------------------------------------STEP1
	@$(shell [ ! -e include ] && mkdir include)
	@$(shell [ ! -e include/config ] && mkdir include/config)
	bash scripts/gen_conf.sh include/config/auto.conf

include/generated/autoconf.h : auto.conf
	@$(shell [ ! -e include/generated ] && mkdir include/generated)
	bash scripts/gen_include.sh include/config/auto.conf $@

conf : include/generated/autoconf.h
	@echo -------------------------------------------STEP2

arch-files :
	make -C arch/$(ARCH)

non-arch-files:
	make -C init
	make -C modules

$(OBJ).elf: conf arch-files non-arch-files
	@echo -------------------------------------------STEP3
	$(LINK) $(LINKFLAGS) $(AR_WARP_BEGIN) arch/$(ARCH)/built-in.a init/built-in.a modules/built-in.a  $(AR_WARP_END) -o $(OBJ).elf
	$(CROSS_COMPILE)nm $(OBJ).elf | sort >> $(OBJ).map
	$(CROSS_COMPILE)objdump -D $(OBJ).elf > $(OBJ).elf.asm

run:
	make -C arch/$(ARCH) run

debug_run:
	make -C arch/$(ARCH) debug_run

debug_gdb:
	make -C arch/$(ARCH) debug_gdb

kill:
	make -C arch/$(ARCH) kill

board_config:
	make -C arch/$(ARCH) board_config

clean:
	make -C init clean
	make -C modules clean
	make -C arch clean
	rm include/config include/generated  $(OBJ)* -rf

PHONY += auto.conf

.PHONY: $(PHONY)
