#########################################################################
# File Name: do.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Sat 09 Apr 2022 03:04:08 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash

arm32_build(){
    make ARCH=arm32 CROSS_COMPILE=arm-linux-gnueabihf- baremetal.elf
}

arm64_build(){
    make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- baremetal.elf
}

rv32_build(){
    make ARCH=rv32 CROSS_COMPILE=riscv64-unknown-elf- baremetal.elf
}

rv64_build(){
    make ARCH=rv64 CROSS_COMPILE=riscv64-unknown-elf- baremetal.elf
}

run(){
    make run
}

debug_run(){
    make debug_run
}

debug_gdb(){
    make debug_gdb
}

kill(){
    make kill
}

board_config(){
    make board_config
}

clean(){
    make clean
}


##########################################################
##########################################################


Usage(){
    cat ${CURRENT_SCRIPT} | grep "(){" | grep -v "^ " \
        | egrep -v  "Usage|Main" \
        | awk -F "(" '{print $1}' \
        | while read line
    do
        echo Usage : ${CURRENT_SCRIPT} ${line}
    done
    exit -1
}

Main(){

    CURRENT_SCRIPT=$0

    if [ $# != 1 ];then
        Usage
    fi

    OBJ=$1

    cat ${CURRENT_SCRIPT}  \
        | grep "(){" \
        | grep -v "^ " \
        | egrep -v  "Usage|Main" \
        | grep -w ${OBJ} > /dev/null
    if [ $? -eq 0 ];then
        ${OBJ}
    else
        echo ${OBJ} : NOT DEFINED
        exit -2
    fi
}

Main $*
