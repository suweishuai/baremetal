#include "shell.h"
#include "stdio.h"
#include "uart.h"
#include "string.h"
#include "parse.h"

#define CNTLQ       0x11
#define CNTLS       0x13
#define DEL         0x7F
#define BACKSPACE   0x08
#define TAB         0x09
#define LF          0x0A
#define CR          0x0D
#define ESC         0x1B


#define PROMPT "suws@baremetel:~$ "
#define PROMPT_DEBUG "suws@debug:~# "

#define INTRO \
"********************\n" \
"****LITTLE SHELL****\n" \
"********************\n"
#define INTRO_DEBUG \
"********************\n" \
"****DEBUG  SHELL****\n" \
"********************\n"



#define CMDLINE_BUF		256
static char oldcmd[CMDLINE_BUF];

static int getline(char *lp, uint32_t n, uint8_t dsp)
{
    uint32_t cnt = 0;
    int c;
    c = -1;
    do {
        c = getchar();
        switch (c) {
            case CNTLQ:                       /* ignore Control S/Q             */
            case CNTLS:
                break;;
            case BACKSPACE:
            case DEL:
                if (cnt == 0) {
                    break;
                }
                cnt--;                         /* decrement count                */
                lp--;                          /* and line pointer               */
                putchar(0x08);                /* echo backspace                 */
                putchar(' ');
                putchar(0x08);
                break;
            case ESC:
                *lp = 0;                       /* ESC - stop editing line        */
                return -1;
            case TAB:
                for(c=0;(oldcmd[c]!=CR)&&(oldcmd[c]!=LF)&&(oldcmd[c]!=0)&&(cnt<60);c++){
                    *lp = oldcmd[c];
                    lp++;
                    cnt++;
                    if(dsp)putchar(oldcmd[c]);
                    else putchar('*');
                }
                break;
            case CR:                          /* CR - done, stop editing line   */
                *lp = c;
                lp++;                          /* increment line pointer         */
                cnt++;                         /* and count                      */
                c = LF;
            default:
                *lp = c;
                if(dsp)putchar(c);             /* echo and store character       */
                else putchar('*');
                lp++;                          /* increment line pointer         */
                cnt++;                         /* and count                      */
                break;
        }
    } while (cnt < n - 2  &&  c != LF);     /* check limit and CR             */
    *lp = 0;                                /* mark end of string             */
    for(c=0;c<=cnt;c++)
        oldcmd[c]= *(lp-cnt+c);
    return 0;
}

static char *get_entry (char *cp, char **pNext) {
    char *sp, lfn = 0, sep_ch = ' ';
    if (cp == NULL) {                          /* skip NULL pointers          */
        *pNext = cp;
        return (cp);
    }
    for ( ; *cp == ' ' || *cp == '\"'; cp++) { /* skip blanks and starting  " */
        if (*cp == '\"') { sep_ch = '\"'; lfn = 1; }
        *cp = 0;
    }
    for (sp = cp; *sp != CR && *sp != LF; sp++) {
        if ( lfn && *sp == '\"') break;
        if (!lfn && *sp == ' ' ) break;
    }
    for ( ; *sp == sep_ch || *sp == CR || *sp == LF; sp++) {
        *sp = 0;
        if ( lfn && *sp == sep_ch) { sp ++; break; }
    }
    *pNext = (*sp) ? sp : NULL;                /* next entry                  */
    return (cp);
}

void shell_user(void)
{
    char *sp,*next,*before;
    int argc = 0;
    char argv[ARGC_MAX][ARGV_LEN_MAX]={0};
    char *argvp[ARGC_MAX];
    int i = 0;
    for ( i = 0 ; i < ARGC_MAX; i++ ){
        argvp[i] = argv[i];
    }
    printf (INTRO);
    while (1) {
        argc = 0;
        printf (PROMPT);
        if (getline(oldcmd, sizeof (oldcmd), 1) != 0) {
            continue;
        }

        before = &oldcmd[0];
        sp = get_entry (before, &next);
        if (*sp == 0) { // enter only
            continue;
        }
        strncpy(argv[0],sp,ARGV_LEN_MAX);
        argc ++;

        while(next){    // have parameter
            before = next;
            sp = get_entry (before, &next);
            strncpy(argv[argc],sp,ARGV_LEN_MAX);
            if (++argc == ARGC_MAX)
                break;
        }

        parse(argvp[0])(argc,argvp);

    }
    return ;
}

void shell_debug(void)
{
    char *sp,*next,*before;
    int argc = 0;
    char argv[ARGC_MAX][ARGV_LEN_MAX]={0};
    char *argvp[ARGC_MAX];
    int i = 0;
    for ( i = 0 ; i < ARGC_MAX; i++ ){
        argvp[i] = argv[i];
    }
    printf (INTRO_DEBUG);
    while (1) {
        argc = 0;
        printf (PROMPT_DEBUG);
        if (getline(oldcmd, sizeof (oldcmd), 1) != 0) {
            continue;
        }

        before = &oldcmd[0];
        sp = get_entry (before, &next);
        if (*sp == 0) { // enter only
            continue;
        }

        strncpy(argv[0],sp,ARGV_LEN_MAX);
        argc ++;

        if (strcmp(argv[0],"quit") == 0)
            break;

        while(next){    // have parameter
            before = next;
            sp = get_entry (before, &next);
            strncpy(argv[argc],sp,ARGV_LEN_MAX);
            if (++argc == ARGC_MAX)
                break;
        }

        parse_debug(argvp[0])(argc,argvp);

    }
    return ;
}
