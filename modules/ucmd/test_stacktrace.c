/*************************************************************************
  > File Name: test_stacktrace.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: 2023年06月04日 星期日 15时04分05秒
 ************************************************************************/

#include "cmd.h"
#include "stacktrace.h"

static int g(int x)
{
    dump_stack();
    return x + 3;
}

static int f(int x)
{
    return g(x);
}

UCMD(stacktrace){
    f(7) + 1;
    return ;
}
