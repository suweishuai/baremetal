/*************************************************************************
  > File Name: compile_optimizer.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 04 May 2022 06:41:44 PM CST
 ************************************************************************/

#include "cmd.h"
#include "stdio.h"
#include "string.h"

#define TEST(FUNC,X)                        \
    do{                                     \
        printf("%s %s begin\n",#FUNC,#X);   \
        extern void test_##FUNC##_##X();    \
        test_##FUNC##_##X();                \
        printf("%s %s end\n",#FUNC,#X);     \
    }while(0)


UCMD(compile_optimiz){
    #include "test_OPTIMIZER.h"
}

UCMD(compile_barrier){
    if (argc != 2){
        printf("Usage for compile_barrier :\n");
        printf("\tcompile_barrier barrier_mem\n");
        printf("\tcompile_barrier barrier_func\n");
        printf("\tcompile_barrier barrier_volatile\n");
        return ;
    }

    if (strcmp(argv[1],"barrier_mem") == 0){
        #include "test_BARRIER_MEM.h"
    }else if (strcmp(argv[1],"barrier_func") == 0){
        #include "test_BARRIER_FUNC.h"
    }else if (strcmp(argv[1],"barrier_volatile") == 0){
        #include "test_BARRIER_VOLATILE.h"
    }
}
