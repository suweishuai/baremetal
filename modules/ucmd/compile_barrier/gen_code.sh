#########################################################################
# File Name: gen_code.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Wed 04 May 2022 08:49:56 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash

FUNC_NAME_FILE=function_name.txt
OPTIMIZER_LEVEL_FILE=optimizer_level.txt
MAKEFILE_OBJ_FILE=Makefile.obj
INCLUDE_DIR=../../../include/


function gen_c_file  {
    func=$1
    optimizer=$2
    file=test_${func}_${optimizer}.c

    # include
    echo "#include \"platform.h\"" > ${file}
    echo "#include \"barrier.h\""  >> ${file}
    echo  >> ${file}

    #diff
    if   [ ${func} == OPTIMIZER ];then
cat << EOF >> $file
#define VOLATILE
#define BARRIER
EOF
    elif [ ${func} == BARRIER_MEM ];then
cat << EOF >> $file
#define VOLATILE
#define BARRIER     \
    do{             \
        barrier();  \
    }while(0)
EOF
    elif [ ${func} == BARRIER_FUNC ];then
cat << EOF >> $file
// __attribute__ ((noinline)) __attribute__((optimize("O0")))
// optimize("O0") 会保证其不被优化,更不会内联
// noinline 会保证其不被内联,但是有可能该函数整个就被干掉了
static void __attribute__((optimize("O0"))) foo(void){};
#define VOLATILE
#define BARRIER     \
    do{             \
        foo();      \
    }while(0)
EOF
    elif [ ${func} == BARRIER_VOLATILE ];then
cat << EOF >> $file
#define VOLATILE volatile
#define BARRIER
EOF
    fi

    echo  >> ${file}
    echo  >> ${file}

    echo "void test_${func}_${optimizer}(void){" >> ${file}

cat << EOF >> $file
    *(VOLATILE unsigned char *)UART_BASE = 'H';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'e';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'l';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'l';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'o';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = ' ';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'W';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'o';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'r';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'l';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = 'd';
    BARRIER;
    *(VOLATILE unsigned char *)UART_BASE = '\n';
EOF
    echo "}" >> ${file}

}

function gen_h_file  {
    func=$1
    optimizer=$2
    file=${INCLUDE_DIR}/test_${func}.h
    echo "TEST(${func},${optimizer});" >> ${file}
}

function gen_makefile  {

    func=$1
    optimizer=$2
    file=Makefile.${func}.${optimizer}
    c_file=test_${func}_${optimizer}.c
    o_file=test_${func}_${optimizer}.o
    asm_file=test_${func}_${optimizer}.asm

    echo "-include ${file}" >> ${MAKEFILE_OBJ_FILE}

    echo "OBJ_ALL += ${o_file}" > ${file}
    echo "${o_file} : ${c_file}" >> ${file}
    echo $'\t'"\$(CROSS_COMPILE)gcc \$(CFLAGS) -${optimizer} -c \$< -o \$@" >> ${file}
    echo $'\t'"\$(CROSS_COMPILE)objdump -j .text -d \$@ > ${asm_file}" >> ${file}
}

function gen {
    echo > ${MAKEFILE_OBJ_FILE}
    cat ${FUNC_NAME_FILE}| while read func
    do
        echo > ${INCLUDE_DIR}/test_${func}.h
        cat ${OPTIMIZER_LEVEL_FILE}| while read optimizer
        do
            gen_c_file ${func} ${optimizer}
            gen_h_file ${func} ${optimizer}
            gen_makefile ${func} ${optimizer}
        done
    done
}

function clean {
    rm  ${MAKEFILE_OBJ_FILE} -f
    cat ${FUNC_NAME_FILE}| while read func
    do
        rm ${INCLUDE_DIR}/test_${func}.h -f
        cat ${OPTIMIZER_LEVEL_FILE}| while read optimizer
        do
            rm test_${func}_${optimizer}.c -f
            rm Makefile.${func}.${optimizer} -f
        done
    done
}


if   [ $1 == gen ];then
    gen
elif [ $1 == clean ];then
    clean
fi
