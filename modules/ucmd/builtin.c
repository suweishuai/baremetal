/*************************************************************************
  > File Name: builtin.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 01 Apr 2022 04:35:11 PM CST
 ************************************************************************/
#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "parse.h"



UCMD(help){

    struct cmd *p = NULL;
    char * tmp = NULL;
    int count = 0;

    p = &__ucmd_start;
    do {
        printf("%2d : %s\n",count+1,p->name);
        tmp = (char *)p;
        tmp +=32;
        p= (struct cmd *)tmp;
        count ++;
    } while (p < &__ucmd_end);

    if(p >= &__ucmd_end)
        printf("total : %d\n",count);
}



UCMD(ct){

    struct cmd *p = NULL;
    char * tmp = NULL;
    int count = 0;

    p = &__ucmd_start;
    do {
        tmp = (char *)p;
        tmp +=32;
        p= (struct cmd *)tmp;
        count ++;
    } while (p < &__ucmd_end);

    if(p >= &__ucmd_end)
        printf("%d\n",count);
}



UCMD(quit){
    printf("%s\n",__func__);
}



UCMD(new){
    printf("%s\n",__func__);
    int i = 0;
    for(; i< argc ; i++){
        printf("Argument %d is %s\n", i, argv[i]);
    }
}

UCMD(exec){
    if (argc == 1) return ;

    parse(argv[1])(--argc,argv+1);
}

UCMD(execs){
    int i = 0 , j = 0;

    for (i = 1 ; i < argc ; i++){

        if (strncmp(argv[i],";",1) == 0 && j != 0  ){
            parse(argv[i-j])(j,argv+i-j);
            j = 0;
        } else
            j++;

    }

    if (j != 0){
        parse(argv[i-j])(j,argv+i-j);
    }

}
