/*************************************************************************
  > File Name: string.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 01 Apr 2022 03:02:53 PM CST
 ************************************************************************/
#include "string.h"

char *strcpy(char *dest, const char *src){
    char *ret = dest;
    while( (*dest++ = *src++) != '\0' );
    return ret;
}

char *strncpy(char *dest, const char *src, size_t n){
    char *ret = dest;

    while( (n-- != 0) &&  (*dest++ = *src++) != '\0');

    if(n == -1) *(dest-1) = '\0';
    return ret;

}

int strcmp(const char *s1, const char *s2)
{
    while(*s1 && (*s1==*s2)){
        ++s1;
        ++s2;
    }
    return *s1 - *s2;
}

int strncmp(const char *s1, const char *s2, size_t n)
{
    while(n--)
    {
        if(*s1 == 0 || *s1 != *s2)
            return *s1 - *s2;
        s1++;
        s2++;
    }
    return 0;
}

uint64_t strtoul(char *s)
{
    uint64_t ret;
    int radix = 10;
    int negative = 0;
    int i;
    ret = 0;
    if(NULL==s)return ret;
    if(*s == '-') {
        negative = 1;
        s++;
    }
    else if(*s == '0') {
        s++;
        if((*s == 'x')||(*s == 'X')){
            s++;
            radix = 0x10;
        }
    }
    else if((*s == 'H')||(*s=='h')) {
        s++;
        radix = 0x10;
    }
    while (*s) {
        if (*s >= '0' && *s <= '9')
            i = *s - '0';
        else if (*s >= 'a' && *s <= 'f')
            i = *s - 'a' + 0xa;
        else if (*s >= 'A' && *s <= 'F')
            i = *s - 'A' + 0xa;
        else
            break;
        if(i >= radix) break;
        ret = (ret * radix) + i;
        s++;

    }
    return negative?(-ret):ret;
}

void *memcpy(void *dest, const void *src, size_t n){

    char *ret = dest;
    while( n-- != 0 )
        *(char *)dest++ = *(char *)src++;
    return ret;
}

void *memset(void *s, int c, size_t n) {
    char *p = (char *)s;
    while (n--) {
        *p++ = c;
    }
    return s;
}
