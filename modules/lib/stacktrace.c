/*************************************************************************
  > File Name: stacktrace.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: 2023年06月04日 星期日 14时40分03秒
 ************************************************************************/

#include "stdio.h"
#include "stacktrace.h"

// arch must provide get_current_stack_frame & get_caller_stack_frame & get_callee_return_address
// get_current_stack_frame provided by arch ,must be inline !!!

#if 1

void dump_stack(void) {
    void * fp_current = get_current_stack_frame();
    void * fp_caller = fp_current;
    void * ret_callee = NULL;
    printf("FP:%p,CALLEE:%08x\n",fp_caller,dump_stack);

    for (;fp_caller;) {
        ret_callee = get_callee_return_address(fp_caller);
        fp_caller = get_caller_stack_frame(fp_caller);
        printf("FP:%p,CALLEE:%08x\n",fp_caller,ret_callee-4);

    }

    return ;
}

#else

void  dump_stack_core(void *fp) {
    void * ret_callee = NULL;
    void * fp_caller = get_caller_stack_frame(fp);
    if (fp_caller != NULL){
        ret_callee = get_callee_return_address(fp_caller);
        dump_stack_core(fp_caller);
    }
    printf("FP:%p,CALLEE:%08x\n",fp_caller,ret_callee == 0 ? ret_callee : ret_callee -4);
    return ;

}

void dump_stack(void) {
    void * fp_current = get_current_stack_frame();
    dump_stack_core(fp_current);
    printf("FP:%p,CALLEE:%08x\n",fp_current,get_callee_return_address(fp_current)-4);
    printf("FP:0x00000000,CALLEE:%08x\n",dump_stack);
    return ;

}

#endif
