/* SPDX-License-Identifier: MIT */

#include "stdio.h"
#include "vsprintf.h"
#include "uart.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))


int snprintf(char *buffer, size_t size, const char *fmt, ...)
{
    va_list args;
    int i;

    va_start(args, fmt);
    i = vsnprintf(buffer, size, fmt, args);
    va_end(args);
    return i;
}

static void uart_write(const void *buf, size_t count){
    size_t i = 0;
    for (;i<count;i++){
         putchar((unsigned int)*((unsigned char*)buf+i));
    }
}

int printf(const char *fmt, ...)
{
    va_list args;
    char buffer[512];
    int i;

    va_start(args, fmt);
    i = vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    uart_write(buffer, min(i, (int)(sizeof(buffer) - 1)));

    return i;
}

int puts(const char * s)
{
    const char * t = s;
    const char * v = s;
    int len = 0;
    int i = 0;

    while(*t != '\0'){
        len++;
        t++;
    }

    for(i=0;i<=len;i++)
        putchar((v[i]));

    putchar('\n');
    return 0;
}
