/*************************************************************************
  > File Name: mem_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 20 Apr 2022 11:23:09 AM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "mem_ops.h"

#define STR(X) #X
#define FUNCTION_WR(name) void name (char *argv1,char *argv2)
#define FUNCTION_RD(name) void name (char *argv1)

void wr64 (char *argv1,char *argv2);
void rd64 (char *argv1);


FUNCTION_WR(wr8){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint8_t data   = (uint8_t)strtoul(argv2);
    write8(addr,data);
}

FUNCTION_RD(rd8){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint8_t data = 0;
    data = read8(addr);
    printf("0x%02x\n",data);
}

FUNCTION_WR(wr16){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint16_t data  = (uint16_t)strtoul(argv2);
    write16(addr,data);
}

FUNCTION_RD(rd16){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint16_t data = 0;
    data = read16(addr);
    printf("0x%04x\n",data);
}

FUNCTION_WR(wr32){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint32_t data  = (uint32_t)strtoul(argv2);
    write32(addr,data);
}

FUNCTION_RD(rd32){

    uint32_t addr  = (uint32_t)strtoul(argv1);
    uint32_t data = 0;
    data = read32(addr);
    printf("0x%08x\n",data);
}


void mem_usage(){
    printf("Usage : mem wr32 0x40000000 0x02\n");
    printf("Usage : mem rd32 0x40000000\n");
    printf("available ops:\n");
    printf("wr64 rd64 wr32 rd32 wr16 rd16 wr8 rd8\n");
}


DCMD(mem){

#define SHOW_MEM(OPS)                                                       \
    do{                                                                     \
        if (strcmp(argv[1],STR(OPS)) == 0 ) {OPS(argv[2]); return;}         \
    }while(0)
#define SET_MEM(OPS)                                                        \
    do {                                                                    \
        if (strcmp(argv[1],STR(OPS)) == 0 ) {OPS(argv[2],argv[3]); return;} \
    }while(0)

    // 0   1    2    3
    // mem wr32 addr data // 4
    // mem rd32 addr      // 3
    if ( strncmp(argv[1],STR(wr),2) == 0 ) {
        if (argc != 4){ mem_usage(); return ; }
        SET_MEM(wr64);
        SET_MEM(wr32);
        SET_MEM(wr16);
        SET_MEM(wr8);
    }else if ( strncmp(argv[1],STR(rd),2) == 0 ) {
        if (argc != 3){ mem_usage(); return ; }
        SHOW_MEM(rd64);
        SHOW_MEM(rd32);
        SHOW_MEM(rd16);
        SHOW_MEM(rd8);
    }
    mem_usage();
}
