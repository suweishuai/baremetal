/*************************************************************************
  > File Name: map_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 15 Jun 2022 01:13:37 PM CST
 ************************************************************************/

#include "stdio.h"
#include "types.h"
#include "string.h"
#include "page.h"
#include "linkage.h"
#include "map.h"

struct pt_alloc_ops pt_ops;
pgd_t idmap_pgd[PTRS_PER_PGD*4] __initdata   __aligned(PAGE_SIZE*4);
pud_t idmap_pud[PTRS_PER_PUD]   __initdata   __aligned(PAGE_SIZE);
pmd_t idmap_pmd[PTRS_PER_PMD]   __initdata   __aligned(PAGE_SIZE);

static inline phys_addr_t __init alloc_pte_early(uintptr_t va)
{
    /*
     * We only create PMD or PGD early mappings so we
     * should never reach here with MMU disabled.
     */
    BUG();
}
static inline pte_t *__init get_pte_virt_early(phys_addr_t pa)
{
    return (pte_t *)((uintptr_t)pa);
}
static phys_addr_t __init alloc_pmd_early(uintptr_t va)
{
    BUG_ON((va - kernel_map.virt_addr) >> PUD_SHIFT);

    return (uintptr_t)idmap_pmd;
}

static pmd_t *__init get_pmd_virt_early(phys_addr_t pa)
{
    /* Before MMU is enabled */
    return (pmd_t *)((uintptr_t)pa);
}

static phys_addr_t __init alloc_pud_early(uintptr_t va)
{
    /* Only one PUD is available for early mapping */
    BUG_ON((va - kernel_map.virt_addr) >> PGDIR_SHIFT);

    return (uintptr_t)idmap_pud;
}

static pud_t *__init get_pud_virt_early(phys_addr_t pa)
{
    return (pud_t *)((uintptr_t)pa);
}

void __init pt_ops_set_early(void)
{
    pt_ops.alloc_pte    = alloc_pte_early;
    pt_ops.get_pte_virt = get_pte_virt_early;
    pt_ops.alloc_pmd    = alloc_pmd_early;
    pt_ops.get_pmd_virt = get_pmd_virt_early;
    pt_ops.alloc_pud    = alloc_pud_early;
    pt_ops.get_pud_virt = get_pud_virt_early;
}

void map_section(void * pgd_base, map_section_t * idmap){
    uintptr_t va, end_va;

    end_va = idmap->virt_start_addr + idmap->size;
    for (va = idmap->virt_start_addr; va < end_va; va += PMD_SIZE)
        create_l1_mapping(
                pgd_base,
                va,
                idmap->phys_start_addr + (va - idmap->virt_start_addr),
                PMD_SIZE,
                PAGE_KERNEL_EXEC);
    return ;
}
