/*************************************************************************
  > File Name: mmu_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 12:50:44 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "barrier.h"
#include "memory.h"
#include "page.h"
#include "platform.h"
#include "map.h"
#include "mmu.h"

DCMD(mmu){
    if (argc != 2){
        printf("Usage for mmu :\n");
        printf("\tmmu on\n");
        printf("\tmmu off\n");
        printf("\tmmu test\n");
        return ;
    }

    // mem
    map_section_t idmap_mem;
    idmap_mem.size              = PHYS_SIZE;
    idmap_mem.phys_start_addr   = PHYS_OFFSET;

    idmap_mem.phys_end_addr     = idmap_mem.phys_start_addr + idmap_mem.size;
    idmap_mem.virt_start_addr   = idmap_mem.phys_start_addr;
    idmap_mem.virt_end_addr     = idmap_mem.phys_end_addr;
    idmap_mem.va_pa_offset      = idmap_mem.virt_start_addr - idmap_mem.phys_start_addr;

    idmap_mem.interval          = SECTION_SIZE;
    idmap_mem.count             = idmap_mem.size/idmap_mem.interval;


    // uart
    map_section_t idmap_uart;
    idmap_uart.size              = SECTION_SIZE;
    idmap_uart.phys_start_addr   = UART_BASE;

    idmap_uart.phys_end_addr     = idmap_uart.phys_start_addr + idmap_uart.size;
    idmap_uart.virt_start_addr   = idmap_uart.phys_start_addr;
    idmap_uart.virt_end_addr     = idmap_uart.phys_end_addr;
    idmap_uart.va_pa_offset      = idmap_uart.virt_start_addr - idmap_uart.phys_start_addr;

    idmap_uart.interval          = SECTION_SIZE;
    idmap_uart.count             = idmap_uart.size/idmap_uart.interval;


    if (strcmp(argv[1],"on") == 0){
        pt_ops_set_early();
        map_section(idmap_pgd,&idmap_mem);
        map_section(idmap_pgd,&idmap_uart);
        arch_map_section(idmap_pgd);
        mmu_on(idmap_pgd);
    }
    else if (strcmp(argv[1],"off") == 0){
        mmu_off();
    }
}
