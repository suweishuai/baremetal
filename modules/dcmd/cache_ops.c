/*************************************************************************
  > File Name: arch_cache_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 15 May 2022 03:48:49 PM CST
 ************************************************************************/

#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "cache_ops.h"
#include "timer.h"
#include "barrier.h"

void cache_test(void){
    uint64_t s=0,e=0,t=0;
    uint32_t high=0,low=0;
    uint32_t i = 0;

    mb();
    icache_off();
    dcache_off();
    mb();
    {
        s = get_time();
        {
            for(i = 0 ;i < 0xffffffff;i++);
        }
        e = get_time();
    }
    t = e-s;
    printf("cache off\t:%x,%x\n",(int32_t)((t>>32)&0xffffffff),(int32_t)(t&0xffffffff));


    mb();
    icache_on();
    dcache_on();
    mb();
    {
        s = get_time();
        {
            for(i = 0 ;i < 0xffffffff;i++);
        }
        e = get_time();
    }
    mb();
    icache_off();
    dcache_off();
    mb();

    t = e-s;
    printf("cache on\t:%x,%x\n",(int32_t)((t>>32)&0xffffffff),(int32_t)(t&0xffffffff));
}


DCMD(cache){
    if (argc != 2){
        printf("Usage for cache :\n");
        printf("\tcache on\n");
        printf("\tcache off\n");
        printf("\tcache test\n");
        return ;
    }
    if (strcmp(argv[1],"on") == 0){
        icache_on();
        dcache_on();
    }
    else if (strcmp(argv[1],"off") == 0){
        icache_off();
        dcache_off();
    }
    else if (strcmp(argv[1],"test") == 0){
        cache_test();
    }
}
