/*************************************************************************
  > File Name: builtin.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Fri 01 Apr 2022 04:35:11 PM CST
 ************************************************************************/
#include "stdio.h"
#include "string.h"
#include "cmd.h"
#include "parse.h"



DCMD(help){

    struct cmd *p = NULL;
    char * tmp = NULL;
    int count = 0;

    p = &__dcmd_start;
    do {
        printf("%2d : %s\n",count+1,p->name);
        tmp = (char *)p;
        tmp +=32;
        p= (struct cmd *)tmp;
        count ++;
    } while (p < &__dcmd_end);

    if(p >= &__dcmd_end)
        printf("total : %d\n",count);
}



DCMD(ct){

    struct cmd *p = NULL;
    char * tmp = NULL;
    int count = 0;

    p = &__dcmd_start;
    do {
        tmp = (char *)p;
        tmp +=32;
        p= (struct cmd *)tmp;
        count ++;
    } while (p < &__dcmd_end);

    if(p >= &__dcmd_end)
        printf("%d\n",count);
}



DCMD(quit){
    // Never come here
}



DCMD(new){
    printf("%s\n",__func__);
    int i = 0;
    for(; i< argc ; i++){
        printf("Argument %d is %s\n", i, argv[i]);
    }
}

DCMD(exec){
    if (argc == 1) return ;

    parse_debug(argv[1])(--argc,argv+1);
}

DCMD(execs){
    int i = 0 , j = 0;

    for (i = 1 ; i < argc ; i++){

        if (strncmp(argv[i],";",1) == 0 && j != 0  ){
            parse_debug(argv[i-j])(j,argv+i-j);
            j = 0;
        } else
            j++;

    }

    if (j != 0){
        parse_debug(argv[i-j])(j,argv+i-j);
    }

}
