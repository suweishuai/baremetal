/*************************************************************************
  > File Name: info.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Wed 11 May 2022 12:05:32 PM CST
 ************************************************************************/

#include "stdio.h"
#include "cmd.h"
#include "string.h"

extern void info_cache(void);
extern void info_mem(void);

DCMD(info){
    if (argc != 2){
        printf("Usage for info :\n");
        printf("\tinfo cache\n");
        printf("\tinfo mem\n");
        return ;
    }
    if (strcmp(argv[1],"cache") == 0){
        info_cache();
    }
    else if (strcmp(argv[1],"mem") == 0){
        info_mem();
    }
}
