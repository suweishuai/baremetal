/*************************************************************************
  > File Name: page_ops.c
  > Author: SuWeishuai
  > Mail: suwsl@foxmail.com
  > Created Time: Sun 05 Jun 2022 08:39:47 PM CST
 ************************************************************************/

#include "stdio.h"
#include "types.h"
#include "string.h"
#include "page.h"
#include "linkage.h"
#include "map.h"

static void __init create_pte_mapping(pte_t *ptep,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot)
{
    uintptr_t pte_idx = pte_index(va);

    BUG_ON(sz != PAGE_SIZE);

    if (pte_none(ptep[pte_idx]))
        ptep[pte_idx] = pfn_pte(PFN_DOWN(pa), prot);
}

void __init create_pmd_mapping(pmd_t *pmdp,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot)
{
    pte_t *ptep;
    phys_addr_t pte_phys;
    pmd_t value;
    static int is_first = 1;
    unsigned long pfn;
    uintptr_t pmd_idx = pmd_index(va);

    if (sz == PMD_SIZE) {
        if (pmd_none(pmdp[pmd_idx])){
            pfn = PFN_DOWN(pa);
            value = pfn_pmd(pfn, prot);
            pmdp[pmd_idx] = value;
        }
        return;
    }

    if (pmd_none(pmdp[pmd_idx])) {
        pte_phys = pt_ops.alloc_pte(va);
        pmdp[pmd_idx] = pfn_pmd(PFN_DOWN(pte_phys), PAGE_TABLE);
        ptep = pt_ops.get_pte_virt(pte_phys);
        if (is_first){
            is_first = 0;
            memset(ptep, 0, PAGE_SIZE);
        }
    } else {
        pte_phys = PFN_PHYS(_pmd_pfn(pmdp[pmd_idx]));
        ptep = pt_ops.get_pte_virt(pte_phys);
    }

    create_pte_mapping(ptep, va, pa, sz, prot);
}

static void __init create_pud_mapping(pud_t *pudp,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot)
{
    pmd_t *nextp;
    phys_addr_t next_phys;
    uintptr_t pud_index = pud_index(va);
    static int is_first = 1;

    if (sz == PUD_SIZE) {
        if (pud_val(pudp[pud_index]) == 0)
            pudp[pud_index] = pfn_pud(PFN_DOWN(pa), prot);
        return;
    }

    if (pud_val(pudp[pud_index]) == 0) {
        next_phys = pt_ops.alloc_pmd(va);
        pudp[pud_index] = pfn_pud(PFN_DOWN(next_phys), PAGE_TABLE);
        nextp = pt_ops.get_pmd_virt(next_phys);
        if (is_first){
            is_first = 0;
            memset(nextp, 0, PAGE_SIZE);
        }
    } else {
        next_phys = PFN_PHYS(_pud_pfn(pudp[pud_index]));
        nextp = pt_ops.get_pmd_virt(next_phys);
    }

    create_pmd_mapping(nextp, va, pa, sz, prot);
}

void __init create_pgd_mapping(pgd_t *pgdp,
        uintptr_t va, phys_addr_t pa,
        phys_addr_t sz, pgprot_t prot)
{
    pgd_next_t *nextp;
    phys_addr_t next_phys;
    static int is_first = 1;
    uintptr_t pgd_idx = pgd_index(va);
    pgd_t value;
    unsigned long pfn;

    if (sz == PGD_SIZE) {
        if (pgd_val(pgdp[pgd_idx]) == 0){
            pfn = PFN_DOWN(pa);
            value = pfn_pgd(pfn, prot);
            pgdp[pgd_idx] = value;
        }
        return;
    }

    if (pgd_val(pgdp[pgd_idx]) == 0) {
        next_phys = alloc_pgd_next(va);
        pgdp[pgd_idx] = pfn_pgd(PFN_DOWN(next_phys), PAGE_TABLE);
        nextp = get_pgd_next_virt(next_phys);
        if (is_first){
            is_first = 0;
            memset(nextp, 0, PAGE_SIZE);
        }
    } else {
        next_phys = PFN_PHYS(_pgd_pfn(pgdp[pgd_idx]));
        nextp = get_pgd_next_virt(next_phys);
    }

    create_pgd_next_mapping(nextp, va, pa, sz, prot);
}
