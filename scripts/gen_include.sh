#########################################################################
# File Name: gen_include.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Sat 09 Apr 2022 01:27:13 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash
INPUT=$1
OUTPUT=$2
# 删掉行首注释
# 删掉 =n 结尾的行
# 行首添加 #define<space>
# 将 行尾=y 换为 =1
# 将 = 换为 <space>"
# 行尾添加 "
# 文件头添加注释



sed '/^#/d' $INPUT          \
   | egrep -v "ASFLAGS|CFLAGS" \
   | sed '/=n$/d'           \
   | sed 's/^/#define &/g'  \
   | sed 's/=y$/=1/g'       \
   | sed 's/=/ \"/g'        \
   | sed 's/$/&"/g'         \
   | sed "1i /*automatically generated c include: don't edit*/" \
   > $OUTPUT
