#########################################################################
# File Name: scripts/gen_conf.sh
# Author: SuWeishuai
# mail: suwsl@foxmail.com
# Created Time: Sat 09 Apr 2022 01:11:14 PM CST
# Version : 1.0
#########################################################################
#!/bin/bash
FILE=$1
cat << EOF > $FILE
# Automatically generated make config: don't edit
ARCH=${ARCH}
CROSS_COMPILE=${CROSS_COMPILE}
EOF

cat arch/${ARCH}/configs/defconfig >> $FILE
